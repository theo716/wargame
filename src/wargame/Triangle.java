package wargame;

/**
 * Classe de gestion des Triangles
 * @author Kevin
 * @version 1.0
 */

public class Triangle {
	
	private final Position[] p = new Position[3];
	
	/**
	 * Contructeur pour un triangle
	 * @param p1 Position
	 * @param p2 Position
	 * @param p3 Position
	 */
	public Triangle(Position p1, Position p2, Position p3) {
		p[0] = p1;
		p[1] = p2;
		p[2] = p3;
	}
	
	/**
	 * Methode qui permet d'avoir la position du triangle
	 * @param n int
	 * @return la position à l'index donne
	 */
	public Position getPosition(int n){
		return p[n];
	}
	
	/**
	 * Methode qui permet de savoir si un triangle est convexe
	 * @return true si convexe sinon false
	 */
	public boolean estConvexe() {
		int i = 0;
		if(getPosition(i).signeAngle(getPosition(i+1), getPosition(i+2)) == Position.SENS.SIAM) {
			return estConvexeSigne(Position.SENS.SIAM);
		}else if(getPosition(i).signeAngle(getPosition(i+1), getPosition(i+2)) == Position.SENS.SAM) {
			return false;
		}
		return false;
	}
	
	/**
	 * Methode qui permet de savoir si un triangle est convexe en fonction d'un certain sens
	 * @param sens Position.SENS
	 * @return true si convexe sinon false
	 */
	private boolean estConvexeSigne(Position.SENS sens) {
		int i;
		for(i=0;i<3-2;i++) {
			if(getPosition(i).signeAngle(getPosition(i+1), getPosition(i+2)) != sens) {
				return false;
			}
		}
		if(getPosition(i).signeAngle(getPosition(i+1), getPosition(0)) != sens) {
			return false;
		}
		if(getPosition(i+1).signeAngle(getPosition(0), getPosition(1)) != sens) {
			return false;
		}
		return true;
	}
}