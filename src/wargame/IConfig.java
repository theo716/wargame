package wargame;

import java.awt.Color;
import java.awt.Toolkit;

/**
 * Classe de l'interface de la Config
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public interface IConfig {
	/**
	 * nombre de pixel d'une case
	 */
	static int NB_PIX_CASE = 36;
	/**
	 * nombre de decoration
	 */
	static int DECORATION = 20;
	/**
	 * Position de la fenetre
	 */
	static int POSITION_X = 100, POSITION_Y = 50;
	/**
	 * Definition de la COULEUR_VISIBLE
	 */
	static Color COULEUR_VISIBLE = Color.white;
	/**
	 * Definition de la COULEUR_INCONNU
	 */
	static Color COULEUR_INCONNU = Color.white;
	/**
	 * Definition de la COULEUR_VIDE
	 */
	static Color COULEUR_VIDE = new Color(59,59,59);
	/**
	 * Definition de la COULEUR_EXPL
	 */
	static Color COULEUR_EXPL = Color.LIGHT_GRAY;
	/**
	 * Definition de la COULEUR_TEXTE, COULEUR_MONSTRES
	 */
	static Color COULEUR_TEXTE = Color.black, COULEUR_MONSTRES = new Color(188,75,81);
	/**
	 * Definition de la COULEUR_HEROS, COULEUR_HEROS_DEJA_JOUE
	 */
	static Color COULEUR_HEROS = new Color(207, 244, 255), COULEUR_HEROS_DEJA_JOUE = Color.pink;
	/**
	 * Definition de la COULEUR_EAU, COULEUR_FORET, COULEUR_ROCHER
	 */
	static Color COULEUR_EAU = new Color(0,119,182), COULEUR_FORET = new Color(52,78,65), COULEUR_ROCHER = new Color(218,215,205);
	/**
	 * Definition de la COULEUR_TOUR
	 */
	static Color COULEUR_TOUR = Color.pink;
	
	/**
	 * Enum des types
	 */
	public static enum types{
		/**
		 * HERO
		 */
		Hero, 
		/**
		 * MONSTRE
		 */
		Monstre, 
		/**
		 * OBSTACLE
		 */
		Obstacle, 
		/**
		 * VIDE
		 */
		Vide
		}
	
	/**
	 * Enum de la visibilite
	 */
	public static enum visibilite{
		/**
		 * VISIBLE
		 */
		Visible, 
		/**
		 * EXPLORE
		 */
		Explore, 
		/**
		 * INCONNU
		 */
		Inconnu
		}
	

	/**
	 * Image elf
	 */
	java.awt.Image elf = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/elf.png"));
	/**
	 * Image nain
	 */
	java.awt.Image nain = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/nain.png"));
	/**
	 * Image humain
	 */
	java.awt.Image humain = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/humain.png"));
	/**
	 * Image hobbit
	 */
	java.awt.Image hobbit = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/hobbit.png"));
	
	/**
	 * Image rocher
	 */
	java.awt.Image rocher = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/rocher.png"));
	/**
	 * Image foret
	 */
	java.awt.Image foret = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/foret.png"));
	/**
	 * Image eau
	 */
	java.awt.Image eau = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/eau.png"));

	/**
	 * Image troll
	 */
	java.awt.Image troll = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/troll.png"));
	/**
	 * Image orc
	 */
	java.awt.Image orc = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/orc.png"));
	/**
	 * Image gobelin
	 */
	java.awt.Image gobelin = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/gobelin.png"));

	/**
	 * Image brouillard
	 */
	java.awt.Image brouillard  = Toolkit.getDefaultToolkit().getImage(IConfig.class.getResource("/img/brouillard.png"));
}
