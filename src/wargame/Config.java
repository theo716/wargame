
package wargame;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Classe de gestion de la Configuration
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Config {
	private static int LARGEUR_CARTE = 15, HAUTEUR_CARTE = 10; // en nombre de cases
	private static int LARGEUR = 1280, HAUTEUR = 780;
	private static int NB_HEROS = 10, NB_MONSTRES = 10, NB_OBSTACLES = 10;
	private static int NB_SOLDATS = NB_HEROS + NB_MONSTRES, NB_ELEMENTS = NB_SOLDATS + NB_OBSTACLES;
	private static boolean etatMusique = true;
	static AudioInputStream audio;
	static DataLine.Info info;
	static Clip musiqueJeu;
	public URL music;
	
	/**
	 * Permet de recuperer les valeurs du fichier de configuration dans config/config.ser et de les appliquer si le fichier existe
	 */
	Config(){
		Properties prop = new Properties();
		
		try {
			try {
				FileInputStream fichier = new FileInputStream("config/config.ser");
				prop.load(fichier);
				fichier.close();
			} catch (IOException e) {
				throw new Exception();
			}
		
			LARGEUR_CARTE = Integer.valueOf(prop.getProperty("lgrCarte"));
			HAUTEUR_CARTE = Integer.valueOf(prop.getProperty("htCarte"));
			LARGEUR = Integer.valueOf(prop.getProperty("lgr"));
			HAUTEUR = Integer.valueOf(prop.getProperty("ht"));
			NB_HEROS = Integer.valueOf(prop.getProperty("heros"));
			NB_MONSTRES = Integer.valueOf(prop.getProperty("mons"));
			NB_OBSTACLES = Integer.valueOf(prop.getProperty("obs"));
			etatMusique = Boolean.valueOf(prop.getProperty("musique"));
		}catch(Exception e){
			System.out.println("Impossible de lire le fichier de configuration");
			System.out.println("Veuillez l'initialiser dans option à partir du menu");
		}
		
		if(LARGEUR_CARTE < 15 || LARGEUR_CARTE > 20) {
			LARGEUR_CARTE = 15;
		}
		if(HAUTEUR_CARTE < 4 || HAUTEUR_CARTE > 15) {
			HAUTEUR_CARTE = 10;
		}
		if(LARGEUR < 400 || LARGEUR > 2000) {
			LARGEUR = 1280;
		}
		if(HAUTEUR < 400 || HAUTEUR > 2000) {
			HAUTEUR = 780;
		}
		if(NB_HEROS < 5 || NB_HEROS > 20) {
			NB_HEROS = 10;
		}
		if(NB_MONSTRES < 5 || NB_MONSTRES > 20) {
			NB_MONSTRES = 10;
		}
		if(NB_OBSTACLES < 5 || NB_OBSTACLES > 20) {
			NB_OBSTACLES = 10;
		}
		
		NB_SOLDATS = NB_HEROS + NB_MONSTRES;
		NB_ELEMENTS = NB_SOLDATS + NB_OBSTACLES;
		
		try {
			music = this.getClass().getResource("/audio.wav");
			System.out.println(music);
		}catch(Exception e) {
			System.out.println("URL: NULL (Musique non trouvable");
			System.out.println("Musique indisponible");
			setetatMusique(false);
		}
		
		if(getetatMusique()==true) {
			try {
				//audio = AudioSystem.getAudioInputStream(new File("audio/audio.wav"));
				audio =  AudioSystem.getAudioInputStream(music);
				
				info = new DataLine.Info(Clip.class, audio.getFormat());
				musiqueJeu = (Clip) AudioSystem.getLine(info);
				musiqueJeu.open(audio);
			} catch (UnsupportedAudioFileException e) {
				setetatMusique(false);
				System.out.println("Impossible de lire le fichier audio");
				System.out.println("Musique indisponible");
			} catch (IOException e) {
				setetatMusique(false);
				System.out.println("Fichier audio introuvable");
				System.out.println("Musique indisponible");
			} catch (Exception e) {
				setetatMusique(false);
				System.out.println("Impossible d'utiliser la musique");
				System.out.println("Musique indisponible");
			}
			System.out.println("Musique disponible");
		}
		
	}

	/**
	 * Methode qui permet d'avoir l'etat de la musique du jeu
	 * @return true si la musique est activee false sinon
	 */
	public static boolean getetatMusique() {
		return etatMusique;
	}
	
	/**
	 * Methode qui permet de modifier l'etat de la musique du jeu
	 * @param etat boolean
	 */
	public static void setetatMusique(boolean etat) {
		etatMusique = etat;
	}

	/** 
	 * Methode qui permet de jouer la musique indefiniment
	 */
	public static void startMusique() {
		musiqueJeu.setFramePosition(0);
		musiqueJeu.loop(Clip.LOOP_CONTINUOUSLY);
		musiqueJeu.start();

	}

	/**
	 * Methode qui permet d'arreter la musique
	 */
	public static void stopMusique() {
		musiqueJeu.stop();
	}

	/**
	 * Methode qui permet de connaitre la largeur de la carte
	 * @return la largeur de la carte en nombre de case
	 */
	public static int getLARGEUR_CARTE() {
		return LARGEUR_CARTE;
	}

	/**
	 * Methode qui permet de modifier la largeur de la carte
	 * @param lARGEUR_CARTE int
	 */
	public static void setLARGEUR_CARTE(int lARGEUR_CARTE) {
		LARGEUR_CARTE = lARGEUR_CARTE;
		if(LARGEUR_CARTE < 15 || LARGEUR_CARTE > 20) {
			LARGEUR_CARTE = 15;
		}
	}

	/**
	 * Methode qui permet de connaitre la hauteur de la carte
	 * @return la hauteur de la carte en nombre de case
	 */
	public static int getHAUTEUR_CARTE() {
		return HAUTEUR_CARTE;
	}

	/**
	 * Methode qui permet de modifier la hauteur de la carte
	 * @param hAUTEUR_CARTE int
	 */
	public static void setHAUTEUR_CARTE(int hAUTEUR_CARTE) {
		HAUTEUR_CARTE = hAUTEUR_CARTE;
		if(HAUTEUR_CARTE < 10 || HAUTEUR_CARTE > 15) {
			HAUTEUR_CARTE = 10;
		}
	}

	/**
	 * Methode qui permet de connaitre la largeur de la fenetre
	 * @return la largeur de la fenetre
	 */
	public static int getLARGEUR() {
		return LARGEUR;
	}

	/**
	 * Methode qui permet de modifier la largeur de la fenetre
	 * @param lARGEUR int
	 */
	public static void setLARGEUR(int lARGEUR) {
		LARGEUR = lARGEUR;
		if(LARGEUR < 400 || LARGEUR > 2000) {
			LARGEUR = 1000;
		}
	}

	/**
	 * Methode qui permet de connaitre la hauteur de la fenetre
	 * @return la hauteur de la fenetre
	 */
	public static int getHAUTEUR() {
		return HAUTEUR;
	}

	/**
	 * Methode qui permet de modifier la hauteur de la fenetre
	 * @param hAUTEUR int
	 */
	public static void setHAUTEUR(int hAUTEUR) {
		HAUTEUR = hAUTEUR;
		if(HAUTEUR < 400 || HAUTEUR > 2000) {
			HAUTEUR = 1000;
		}
	}

	/**
	 * Methode qui permet de connaitre le nombre de Heros
	 * @return le nombre de heros
	 */
	public static int getNB_HEROS() {
		return NB_HEROS;
	}

	/**
	 * Methode qui permet de modifier le nombre de heros
	 * @param nB_HEROS int
	 */
	public static void setNB_HEROS(int nB_HEROS) {
		NB_HEROS = nB_HEROS;
		if(NB_HEROS < 5 || NB_HEROS > 20) {
			NB_HEROS = 10;
		}
		setNB_ELEMENTS();
	}

	/**
	 * Methode qui permet de connaitre le nombre de monstres
	 * @return le nombre de monstres
	 */
	public static int getNB_MONSTRES() {
		return NB_MONSTRES;
	}

	/**
	 * Methode qui permet de modifier le nombre de monstres
	 * @param nB_MONSTRES int
	 */
	public static void setNB_MONSTRES(int nB_MONSTRES) {
		NB_MONSTRES = nB_MONSTRES;
		if(NB_MONSTRES < 5 || NB_MONSTRES > 20) {
			NB_MONSTRES = 10;
		}
		setNB_ELEMENTS();
	}

	/**
	 * Methode qui permet de connaitre le nombre d'obstacles
	 * @return le nombre d'obstacle
	 */
	public static int getNB_OBSTACLES() {
		return NB_OBSTACLES;
	}

	/**
	 * Methode qui permet de modifier le nombre d'obstacles
	 * @param nB_OBSTACLES int
	 */
	public static void setNB_OBSTACLES(int nB_OBSTACLES) {
		NB_OBSTACLES = nB_OBSTACLES;
		if(NB_OBSTACLES < 5 || NB_OBSTACLES > 20) {
			NB_OBSTACLES = 10;
		}
		setNB_ELEMENTS();
	}

	/**
	 * Methode qui permet de connaitre le nombre de soldats
	 * @return le nombre de soldats
	 */
	public static int getNB_SOLDATS() {
		return NB_SOLDATS;
	}

	/**
	 * Methode qui permet de modifier le nombre de soldats
	 */
	private static void setNB_SOLDATS() {
		NB_SOLDATS = NB_HEROS + NB_MONSTRES;
	}

	/**
	 * Methode qui permet de connaitre le nombre d'elements
	 * @return le nombre d'elements
	 */
	public static int getNB_ELEMENTS() {
		return NB_ELEMENTS;
	}

	/**
	 * Methode qui permet de modifier le nombre d'elements
	 */
	private static void setNB_ELEMENTS() {
		setNB_SOLDATS();
		NB_ELEMENTS = NB_SOLDATS + NB_OBSTACLES;
	}
	
	
}