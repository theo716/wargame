package wargame;

/**
 * Classe de l'interface des Soldats
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public interface ISoldat {
	
	/**
	 * Enum TypesHeros
	 */
   static enum TypesH {
	   /**
	    * HUMAIN
	    */
      HUMAIN (40,3,10,2), 
      /**
	    * NAIN
	    */
      NAIN (80,1,20,0), 
      /**
	    * ELF
	    */
      ELF (70,5,10,6), 
      /**
	    * HOBBIT
	    */
      HOBBIT (20,3,5,2);
	   /**
	    * POINTS_DE_VIE = pdv , PORTEE_VISUELLE = portee, PUISSANCE = puissance attaque,TIR = portee de tir
	    */
      private final int POINTS_DE_VIE, PORTEE_VISUELLE, PUISSANCE, TIR;
      
      /**
       * Constructeur de TypesH
       * @param points int
       * @param portee int
       * @param puissance int 
       * @param tir int
       */
      TypesH(int points, int portee, int puissance, int tir) {
POINTS_DE_VIE = points; PORTEE_VISUELLE = portee;
PUISSANCE = puissance; TIR = tir;
      }
      
      /**
       * Permet de connaitre le nom du type
       * @return String
       */
      public String getNomType() {return "";}
      
      /**
       * Permet de connaitre le nombre de points de vie
       * @return int
       */
      public int getPoints() { return POINTS_DE_VIE; }

      /**
       * Permet de connaitre la portee
       * @return int
       */
      public int getPortee() { return PORTEE_VISUELLE; }
      
      /**
       * Permet de connaitre la puissance
       * @return int
       */
      public int getPuissance() { return PUISSANCE; }
      
      /**
       * Permet de connaitre le tir
       * @return int
       */
      public int getTir() { return TIR; }
      
      /**
       * Methode qui permet d'avoir un type alea
       * @return TypesH
       */
      public static TypesH getTypeHAlea() {
         return values()[(int)(Math.random()*values().length)];
      }
   }
   
   	/**
	 * Enum TypesMonstre
	 */
   public static enum TypesM {
	   /**
	    * TROLL
	    */
      TROLL (100,1,30,0), 
      /**
	    * ORC
	    */
      ORC (40,2,10,3), 
      /**
	    * GOBELIN
	    */
      GOBELIN (20,2,5,2);
	   
	   /**
	    * POINTS_DE_VIE = pdv , PORTEE_VISUELLE = portee, PUISSANCE = puissance attaque,TIR = portee de tir
	    */
      private final int POINTS_DE_VIE, PORTEE_VISUELLE, PUISSANCE, TIR;
      /**
       * Constructeur de TypesM
       * @param points int
       * @param portee int
       * @param puissance int 
       * @param tir int
       */
      TypesM(int points, int portee, int puissance, int tir) {
POINTS_DE_VIE = points; PORTEE_VISUELLE = portee;
PUISSANCE = puissance; TIR = tir;
      }
      /**
       * Permet de connaitre le nom du type
       * @return String
       */
      public String getNomType() {return "";}
      
      /**
       * Permet de connaitre le nombre de points de vie
       * @return int
       */
      public int getPoints() { return POINTS_DE_VIE; }
      
      /**
       * Permet de connaitre le nombre de points de vie MAX
       * @return int
       */
      public int getPointsMax() {return POINTS_DE_VIE;};
      
      /**
       * Permet de connaitre la portee
       * @return int
       */
      public int getPortee() { return PORTEE_VISUELLE; }
      
      /**
       * Permet de connaitre la puissance
       * @return int
       */
      public int getPuissance() { return PUISSANCE; }
      
      /**
       * Permet de connaitre le tir
       * @return int
       */
      public int getTir() { return TIR; } 
      
      /**
       * Methode qui permet d'avoir un type alea
       * @return TypesH
       */
      public static TypesM getTypeMAlea() {
         return values()[(int)(Math.random()*values().length)];
      }
   }
   
   /**
    * Permet de connaitre le tour
    * @return int
    */
   int getTour();
   
   /**
    * Permet de jouer un tour
    * @param tour int
    */
   void joueTour(int tour);
   
   /**
    * Permet de faire un combat
    * @param soldat Soldat
    * @param c Carte
    */
   void combat(Soldat soldat, Carte c);
   
   /**
    * Permet de deplacer un soldat
    * @param newPos Position
    */
   void seDeplace(Position newPos);
}