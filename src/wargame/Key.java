package wargame;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;

/**
 * Classe de gestion de la classe Key
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Key implements KeyListener {
		private static JDialog d;
		private FenetreJeu f;
		Date startPauseTime = new Date();
		Date endTime ;
		long duration;
		
		/**
		 * Constructeur de la classe Key
		 * @param fen FenetreJeu
		 */
		public Key(FenetreJeu fen){
			f = fen;
		}
		public void keyTyped(KeyEvent e) {
			
		    
		}
		

		public void keyReleased(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			/* on capture le code de la touch ESC*/
			if(e.getKeyCode() == 27 && f.pause == true) {
				startPauseTime = new Date();
				/*creer une fentre pour utiliser le Jdialog*/
				JFrame fenetre = new JFrame();
				fenetre.setLocationRelativeTo(null);
				
				/*creation du Jdialog*/
				d = new JDialog(fenetre , "PAUSE", true);
				d.setLayout( new FlowLayout() );
				
				/*creation du boutton reprendre*/
				Button reprendre = new Button("Reprendre");
				reprendre.setPreferredSize(new Dimension (300, 150));
				reprendre.setForeground(Color.black);
				reprendre.setBackground(Color.gray);
				reprendre.setFont(new Font("Arial", Font.BOLD, 18));
				reprendre.setSize(180,60);

				/*création du boutton quitter*/
				Button quitter = new Button("Quitter");
				quitter.setSize(180, 60);
				quitter.setPreferredSize(new Dimension (300, 150));
				quitter.setForeground(Color.black);
				quitter.setBackground(Color.gray);
				quitter.setFont(new Font("Arial", Font.BOLD, 18));
				quitter.setSize(180,60);
				
				/*création du boutton sauvegarde*/
				Button sauvegarde = new Button("Sauvegarder");
				sauvegarde.setSize(180, 60);
				sauvegarde.setPreferredSize(new Dimension (300, 150));
				sauvegarde.setForeground(Color.black);
				sauvegarde.setBackground(Color.gray);
				sauvegarde.setFont(new Font("Arial", Font.BOLD, 18));
				sauvegarde.setSize(180,60);
				
				sauvegarde.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						JDialog dialog = new JDialog(f, "Sauvegardes", true);
						dialog.setLayout(new BoxLayout(dialog.getContentPane(), BoxLayout.PAGE_AXIS));
						dialog.setSize(400, 400);
						
						File rep = new File("save");
						JLabel titre = new JLabel("Sauvegardes");
						JFormattedTextField input = new JFormattedTextField();
						
						JButton valider = new JButton("Valider");
						JList<String> liste;

						
						
						if(rep.isDirectory() == false && rep.exists() == true) {
							JOptionPane.showMessageDialog(dialog, "Impossible de creer le repertoire des sauvegardes car un fichier de meme nom existe", "Erreur", JOptionPane.ERROR_MESSAGE);
							return;
						}
						
						if(rep.exists() == false) {
							rep.mkdir();
						}
						liste = new JList<String>(rep.list());
						
						input.setSize(200, 60);
						input.setMaximumSize(new Dimension(200, 60));
						dialog.getContentPane().add(titre);
						if(liste.getModel().getSize() == 0) {
							dialog.getContentPane().add(new JLabel("Aucun fichier de sauvegarde n'a ete cree"));
						}else {
							dialog.getContentPane().add(liste);
						}
							
							valider.setSize(180, 60);
							valider.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {

									FileOutputStream f;
									ObjectOutputStream oos;
									if(input.getText().length() == 0) {
										JOptionPane.showMessageDialog(dialog, "Le nom du fichier ne peut pas etre vide", "Erreur", JOptionPane.ERROR_MESSAGE);
										return;
									}else if(Pattern.matches("[\\/:*?\"<>|]", input.getText()) == true) {
										JOptionPane.showMessageDialog(dialog, "Le nom de fichier ne peut pas contenir les caractères suivants : \\/:*?\"<>|", "Erreur", JOptionPane.ERROR_MESSAGE);
										return;
									}
									
									for(String fic : rep.list()) {
										if(fic.equals(input.getText())) {
											if(JOptionPane.showConfirmDialog(null, "Un fichier porte dejà ce nom, voulez-vous l'ecraser?", "Attention", JOptionPane.YES_NO_OPTION,
											JOptionPane.QUESTION_MESSAGE) != 0) {
												return;
											}else {
												break;
											}
										}
									}
									try {
										PanneauJeu.carte.save();
										
										f = new FileOutputStream("save/" + input.getText());
										oos = new ObjectOutputStream(f);
										oos.writeObject(PanneauJeu.carte);
										oos.flush();
										oos.close();
										PanneauJeu.carte.restaure();//uniquement dans ce cas car la partie continue normalement
									} catch (FileNotFoundException e1) {
										e1.printStackTrace();
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									dialog.dispose();
									
								}
								
							});
							
							liste.addMouseListener(new MouseListener() {
								public void mouseClicked(MouseEvent e) {
									input.setText(liste.getSelectedValue().toString());
								}
								public void mousePressed(MouseEvent e) {
								}
								public void mouseReleased(MouseEvent e) {
								}
								public void mouseEntered(MouseEvent e) {
								}
								public void mouseExited(MouseEvent e) {
								}
							});
							
							dialog.getContentPane().add(input);
							dialog.getContentPane().add(valider);
						
						
						
						dialog.setVisible(true);
					}
				});
				
				/*Reprendre le jeu */
				reprendre.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						endTime = new Date();
					    duration = endTime.getTime() - startPauseTime.getTime();
					    PanneauJeu.carte.setDuration(duration + PanneauJeu.carte.getDuration());
						d.setVisible(false);
					}
				});
				
				/*retourner au menu principale*/
				quitter.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						PanneauJeu.carte = null;
						/* On stop la musique */
						if(Config.getetatMusique()) {
							Config.stopMusique();
						}
						d.setVisible(false);
						f.pause = false;
						f.getCl().show(f.getJp(), "Menu");
					}
				});
				
				/*ajout des elements dans le dialog*/
				d.add(reprendre);
				d.add(sauvegarde);
				d.add(quitter);
				d.setSize(350,600);
				d.setVisible(true);
				
			    
			}
			
		}
		
}
