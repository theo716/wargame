package wargame;
import java.awt.Color;

/**
 * Classe de gestion des Obstacles
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Obstacle extends Element {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Enum pour connaitre le type d'obstacle
	 */
	public enum TypeObstacle {
		/**
		 * ROCHER
		 */
		ROCHER (COULEUR_ROCHER), 
		/**
		 * FORET
		 */
		FORET (COULEUR_FORET), 
		/**
		 * EAU
		 */
		EAU (COULEUR_EAU);
		
		/**
		 * Variable couleur
		 */
		private final Color COULEUR;
		
		/**
		 * Constructeur d'un obstacle
		 * @param couleur Color
		 */
		TypeObstacle(Color couleur) { COULEUR = couleur; }
		
		/**
		 * Methode qui permet d'obtenir un obstacle aleatoire
		 * @return TypeObstacle
		 */
		public static TypeObstacle getObstacleAlea() {
			return values()[(int)(Math.random()*values().length)];
		}
	}
	
	/**
	 * TypeObstacle TYPE
	 */
	private TypeObstacle TYPE;
	
	/**
	 * Constructeur de la classe Obstacle
	 * @param pos Position
	 */
	Obstacle(Position pos) {
		super(types.Obstacle, pos);
		TYPE = TypeObstacle.getObstacleAlea();
	}
	
	/**
	 * Redefinition de la methode toString d'un obstacle
	 * @return String
	 */
	public String toString() {
		return ""+TYPE;
	}
	
	/**
	 * Methode qui permet de connaitre la couleur de l'obstacle
	 * @return Color
	 */
	public Color getColor() {
		return this.TYPE.COULEUR;
	}
}