package wargame;

/**
 * Classe de gestion des Monstres
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Monstre extends Soldat {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * TypesM monstre
	 */
	private TypesM monstre;
	
	/**
	 * Constructeur de la classe Monstre
	 * @param pos Position
	 */
	public Monstre (Position pos) {
		super(types.Monstre, pos);
		monstre = TypesM.getTypeMAlea();
		setPoints(monstre.getPoints());
	}
		
	/**
	 * Methode qui permet de connaitre le nom du type du monstre
	 * @return String (TROLL ou ORC ou GOBELIN)
	 */
	public String getNomType() {
		switch(this.getPointsMax()) {
			case 100:
				return "TROLL";
			case 40:
				return "ORC";
			case 20:
				return "GOBELIN";
		}
		return "MONSTRE";
	}
	
	/**
	 * Methode qui permet de connaitre le nombre de points maximum d'un monstre
	 * @return int
	 */
	public int getPointsMax() {
		return 	monstre.getPointsMax();
	}
	
	/**
	 * Methode qui permet de connaitre la portee d'un monstre
	 * @return int
	 */
    public int getPortee() {
    	return monstre.getPortee();
    }
    
    /**
     * Methode qui permet de connaitre la puissance d'un monstre
     * @return int
     */
    public int getPuissance() {
    	return monstre.getPuissance();
    }
    
    /**
     * Methode qui permet de connaitre le tir d'un monstre
     * @return int
     */
    public int getTir() {
    	return monstre.getTir();
    }

}