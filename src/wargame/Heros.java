package wargame;

/**
 * Classe de gestion des Heros
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Heros extends Soldat {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * TypesH hero
	 */
	private TypesH hero;
	
	/**
	 * Constructeur de la classe Heros
	 * @param pos Position
	 */
	public Heros (Position pos) {
		super(types.Hero, pos);
		hero = TypesH.getTypeHAlea();
		setPoints(this.getPoints());
	}

	/**
	 * Methode qui permet de connaitre les Points du Heros
	 * @return int
	 */
	public int getPoints() {
    	return hero.getPoints();
    }
	
	/**
	 * Methode qui permet de connaitre la portee du Heros
	 * @return int
	 */
    public int getPortee() {
    	return hero.getPortee();
    }
    
    /**
     * Methode qui permet de connaitre la puissance du Heros
     * @return int
     */
    public int getPuissance() {
    	return hero.getPuissance();
    }
    
    /**
     * Methode qui permet de connaitre le tir du Heros
     * @return int
     */
    public int getTir() {
    	return hero.getTir();
    }
    
}
