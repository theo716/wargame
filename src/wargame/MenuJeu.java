package wargame;

import java.awt.Button;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * Classe de gestion du Menu Jeu
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class MenuJeu extends JPanel implements IConfig {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Carte pour le menu
	 */
	private Carte carte = new Carte(1);
	
	/**
	 * JPanel conteneur
	 */
	private JPanel conteneur = this;
	
	/**
	 * JPanel pour les boutons
	 */
	private JPanel bt = new JPanel();
	
	/**
	 * JPanel pour la sauvegarde
	 */
	private JPanel save = new JPanel();
	
	/**
	 * JPanel op
	 */
	private JPanel op = new JPanel();
	
	/**
	 * JPanel btParent
	 */
	private JPanel btParent = new JPanel();
	
	/**
	 * JPanel saveParent
	 */
	private JPanel saveParent = new JPanel();
	
	/**
	 * JPanel opParent
	 */
	private JPanel opParent = new JPanel();
	
	/**
	 * CardLayout du Menu
	 */
	private CardLayout cl = new CardLayout();
	
	/**
	 * Couleur du fonds
	 */
	private Color backGround = getBackground();
	
	/**
	 * liste du module JList
	 */
	private JList<Object> liste;
	
	/**
	 * bouton pour rentrer dans le menu de sauvegarde
	 */
	private Button charger;

	/**
	 * bouton pour sortir du menu de sauvegarde
	 */
	private Button sortirSave;
	
	/**
	 * boolean pour connaitre l'etat de la musique
	 */
	private boolean etatMusique;
	
	
	/**
	 * Constructeur de la classe MenuJeu
	 * @param f FenetreJeu
	 */
	MenuJeu(FenetreJeu f){
		btParent.setOpaque(false);
		opParent.setOpaque(false);
		saveParent.setOpaque(false);
		
		setLayout(cl);
		add(btParent, "Menu");
		add(opParent, "Option");
		add(saveParent, "Save");
		
		btParent.add(bt);
		opParent.add(op);
		saveParent.add(save);
		
		cl.show(this, "Menu");
		
		bt.setLayout(new BoxLayout(bt,BoxLayout.PAGE_AXIS));
		op.setLayout(new BoxLayout(op,BoxLayout.PAGE_AXIS));
		save.setLayout(new BoxLayout(save,BoxLayout.PAGE_AXIS));

		/*ce bouton permet de demarrer un nouveau jeu*/
		Button demarrer = new Button("Demarrer un nouveau jeu");
		demarrer.setForeground(Color.black);
		demarrer.setBackground(Color.BLUE);
		demarrer.setPreferredSize(new Dimension (300, 150));
		demarrer.setFont(new Font("Arial", Font.BOLD, 18));
		demarrer.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		f.setPj(new PanneauJeu(f, null));
	    		f.pause = true;
	    		f.getJp().add(f.getPj(), "Jeu");
	    		f.conteneur = 1;
			}
		});
		bt.add(demarrer);
		bt.add(Box.createRigidArea(new Dimension(0,10)));
		
		/*ce bouton permet d'acceder aux sauvegardes*/
		Button sauvegarde = new Button("Sauvegardes");
		sauvegarde.setPreferredSize(new Dimension (300, 150));
		sauvegarde.setForeground(Color.black);
		sauvegarde.setBackground(Color.red);
		sauvegarde.setFont(new Font("Arial", Font.BOLD, 18));

		File rep = new File("save");
		JFormattedTextField input = new JFormattedTextField();
		input.setEditable(false);
		sauvegarde.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		int i;
	    		File file;
	    		List<String> listeFile = new ArrayList<String>();
	    		f.pause = false;
	    		cl.show(conteneur, "Save");

	    		
	    		
	    		if(rep.isDirectory() == false && rep.exists() == true) {
	    			JOptionPane.showMessageDialog(save, "Impossible d'acceder au repertoire des sauvegardes car celui ci n'existe pas", "Erreur", JOptionPane.ERROR_MESSAGE);
	    			return;
	    		}
	    		
	    		if(rep.exists() == false) {
	    			rep.mkdir();
	    			return;
	    		}
	    		
	    		for(i=0;i<rep.list().length;i++) {
	    			file = rep.listFiles()[i];
	    			if(file.isFile()) {
	    				listeFile.add(file.getName());
	    			}
	    		}
	    		
	    		liste = new JList<Object>(listeFile.toArray());
	    		
	    		save.remove(input);
	    		save.remove(charger);
	    		save.remove(sortirSave);
	    		
	    		if(liste.getModel().getSize() == 0) {
	    			save.add(new JLabel("Aucun fichier de sauvegarde n'a ete cree"));
	    		}else {
	    			save.add(liste);
	    		}
	    		
	    		save.add(input);
	    		save.add(charger);
	    		save.add(sortirSave);
				
				liste.addMouseListener(new MouseListener() {
					public void mouseClicked(MouseEvent e) {
						input.setText(liste.getSelectedValue().toString());
					}
					public void mousePressed(MouseEvent e) {
					}
					public void mouseReleased(MouseEvent e) {
					}
					public void mouseEntered(MouseEvent e) {
					}
					public void mouseExited(MouseEvent e) {
					}
				});
			}
		});
		bt.add(sauvegarde);
		bt.add(Box.createRigidArea(new Dimension(0,10)));
		
		/*
		 * Creation de la partie sauvegarde
		 */
		JLabel titre = new JLabel("Sauvegardes");
		charger = new Button("Valider");
		
		charger.setFont(new Font("Arial", Font.BOLD, 18));
		charger.setForeground(Color.black);
		charger.setBackground(Color.green);
		charger.setPreferredSize(new Dimension (300, 150));
		input.setSize(200, 60);
		save.add(titre);
		
		charger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String fichier = null;
				FileInputStream fi;
				ObjectInputStream oos;
				if(input.getText().length() == 0) {
					JOptionPane.showMessageDialog(save, "Le nom du fichier ne peut pas etre vide", "Erreur", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				for(String fic : rep.list()) {
					if(fic.equals(input.getText())) {
						fichier = fic;
						break;
					}
				}
				if(fichier == null) {
					JOptionPane.showMessageDialog(save, "Le nom du fichier ne correspond pas à un fichier des fichiers de sauvegarde", "Erreur", JOptionPane.ERROR_MESSAGE);
					return;
				}
				try {
					fi = new FileInputStream("save/" + fichier);
					oos = new ObjectInputStream(fi);
		    		f.setPj(new PanneauJeu(f, (Carte)oos.readObject()));
                                f.pause = true;
		    		f.getJp().add(f.getPj(), "Jeu");
		    		f.conteneur = 2;
					oos.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				
				
			}
			
		});
		
		/*ce bouton permet d'acceder aux options*/
		Button option = new Button("Options");
		option.setPreferredSize(new Dimension (300, 150));
		option.setFont(new Font("Arial", Font.BOLD, 18));
		option.setForeground(Color.black);
		option.setBackground(Color.green);
		option.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		f.pause = false;
	    		cl.show(conteneur, "Option");
			}
		});
		bt.add(option);
		bt.add(Box.createRigidArea(new Dimension(0,10)));
		
		/*ce bouton permet de quitter le jeu*/
		Button quitter = new Button("Quitter");
		quitter.setPreferredSize(new Dimension (300, 150));
		quitter.setFont(new Font("Arial", Font.BOLD, 18));
		quitter.setForeground(Color.black);
		quitter.setBackground(Color.pink);
		quitter.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		 f.dispose();
			}
		});
		bt.add(quitter);
		
		
		/*
		 * creation de la partie option
		 * debut de la partie pour parametrer la taille de la fenetre
		 */
		//hauteur de la jframe
		JPanel hauteur = new JPanel();
		hauteur.setLayout(new BoxLayout(hauteur,BoxLayout.LINE_AXIS));
		hauteur.setPreferredSize(new Dimension (300, 40));
		hauteur.setFont(new Font("Arial", Font.BOLD, 18));
		op.add(hauteur);
		
		JLabel labelHt = new JLabel("Hauteur : ");
		JFormattedTextField formatHt = new JFormattedTextField();
		formatHt.setText(Integer.toString(Config.getHAUTEUR()));
		hauteur.add(labelHt);
		hauteur.add(formatHt);
		
		//largeur de la jframe
		JPanel largeur = new JPanel();
		largeur.setLayout(new BoxLayout(largeur,BoxLayout.LINE_AXIS));
		largeur.setPreferredSize(new Dimension (300, 40));
		largeur.setFont(new Font("Arial", Font.BOLD, 18));
		op.add(largeur);
		
		JLabel labelLgr = new JLabel("Largeur : ");
		JFormattedTextField formatLgr = new JFormattedTextField();
		formatLgr.setText(Integer.toString(Config.getLARGEUR()));
		largeur.add(labelLgr);
		largeur.add(formatLgr);

		/*
		 * fin de la partie taille de la fenetre
		 * debut de la partie pour parametrer la taille de la carte
		 */
		//hauteur de la carte
		JPanel hauteurCarte = new JPanel();
		hauteurCarte.setLayout(new BoxLayout(hauteurCarte,BoxLayout.LINE_AXIS));
		hauteurCarte.setPreferredSize(new Dimension (300, 40));
		hauteurCarte.setFont(new Font("Arial", Font.BOLD, 18));
		op.add(hauteurCarte);
		
		JLabel labelHtCarte = new JLabel("Hauteur de la Carte : ");
		JFormattedTextField formatHtCarte = new JFormattedTextField();
		formatHtCarte.setText(Integer.toString(Config.getHAUTEUR_CARTE()));
		hauteurCarte.add(labelHtCarte);
		hauteurCarte.add(formatHtCarte);
		
		//largeur de la carte
		JPanel largeurCarte = new JPanel();
		largeurCarte.setLayout(new BoxLayout(largeurCarte,BoxLayout.LINE_AXIS));
		largeurCarte.setPreferredSize(new Dimension (300, 40));
		largeurCarte.setFont(new Font("Arial", Font.BOLD, 18));
		op.add(largeurCarte);
		
		JLabel labelLgrCarte = new JLabel("Largeur de la Carte : ");
		JFormattedTextField formatLgrCarte = new JFormattedTextField();
		formatLgrCarte.setText(Integer.toString(Config.getLARGEUR_CARTE()));
		largeurCarte.add(labelLgrCarte);
		largeurCarte.add(formatLgrCarte);

		/*
		 * fin de la partie taille de la carte
		 * debut de la partie pour parametrer le nombre des elements
		 */
		//nombre de heros
		JPanel heros = new JPanel();
		heros.setLayout(new BoxLayout(heros,BoxLayout.LINE_AXIS));
		heros.setPreferredSize(new Dimension (300, 40));
		heros.setFont(new Font("Arial", Font.BOLD, 18));
		op.add(heros);
		
		JLabel labelHeros = new JLabel("Nombre de heros : ");
		JFormattedTextField formatHeros = new JFormattedTextField();
		formatHeros.setText(Integer.toString(Config.getNB_HEROS()));
		heros.add(labelHeros);
		heros.add(formatHeros);
		
		//nombre de monstres
		JPanel monstre = new JPanel();
		monstre.setLayout(new BoxLayout(monstre,BoxLayout.LINE_AXIS));
		monstre.setPreferredSize(new Dimension (300, 40));
		monstre.setFont(new Font("Arial", Font.BOLD, 18));
		op.add(monstre);
		
		JLabel labelMonstre = new JLabel("Nombre de monstres : ");
		JFormattedTextField formatMonstre = new JFormattedTextField();
		formatMonstre.setText(Integer.toString(Config.getNB_MONSTRES()));
		monstre.add(labelMonstre);
		monstre.add(formatMonstre);
		
		//nombre d'obstacles
		JPanel obstacle = new JPanel();
		obstacle.setLayout(new BoxLayout(obstacle,BoxLayout.LINE_AXIS));
		obstacle.setPreferredSize(new Dimension (300, 40));
		obstacle.setFont(new Font("Arial", Font.BOLD, 18));
		op.add(obstacle);
		
		JLabel labelObstacle = new JLabel("Nombre d'obstacles : ");
		JFormattedTextField formatObstacle = new JFormattedTextField();
		formatObstacle.setText(Integer.toString(Config.getNB_OBSTACLES()));
		obstacle.add(labelObstacle);
		obstacle.add(formatObstacle);
		
		//son oui ou non
		JPanel son = new JPanel();
		son.setLayout(new BoxLayout(son,BoxLayout.LINE_AXIS));
		son.setPreferredSize(new Dimension (300, 40));
		son.setFont(new Font("Arial", Font.BOLD, 18));
		op.add(son);
		
		JLabel labelson = new JLabel("Son: ");
		
		ButtonGroup groupe = new ButtonGroup();
		
		JRadioButton caseOui = new JRadioButton("oui");
		JRadioButton caseNon = new JRadioButton("non");
		
		if(Config.getetatMusique()) {
			caseOui.setSelected(true);
		}else {
			caseNon.setSelected(true);
		}
		
		son.add(labelson);
		
		groupe.add(caseOui);
		groupe.add(caseNon);
		
		son.add(caseOui);
		son.add(caseNon);
		
		caseOui.addItemListener(new ItemListener() {
		      public void itemStateChanged(ItemEvent e) {
		        etatMusique = true;
		      }
		    });
		
		caseNon.addItemListener(new ItemListener() {
		      public void itemStateChanged(ItemEvent e) {
		    	  etatMusique = false;
		      }
		    });
		
		/*ce bouton permet de sortir des sauvegardes*/
		sortirSave = new Button("Sortir");
		sortirSave.setPreferredSize(new Dimension (300, 150));
		sortirSave.setFont(new Font("Arial", Font.BOLD, 18));
		sortirSave.setForeground(Color.black);
		sortirSave.setBackground(Color.pink);
		sortirSave.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		save.remove(liste);
	    		cl.show(conteneur, "Menu");
			}
		});
		save.add(sortirSave);
		
		/*
		 * fin de la partie option
		 */
		
		/*ce bouton permet de valider les modifications des options*/
		Button valider = new Button("Valider");
		valider.setPreferredSize(new Dimension (300, 150));
		valider.setFont(new Font("Arial", Font.BOLD, 18));
		valider.setForeground(Color.black);
		valider.setBackground(Color.green);
		valider.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		Properties prop = new Properties();
	    		int valeur;
	    		String texte;
	    		
	    		try {
	    			texte = formatHt.getText();
	    			valeur = Integer.valueOf(texte);
	    			if(valeur >= 400 && valeur <= 2000) {
	    				Config.setHAUTEUR(valeur);
	    				prop.setProperty("ht", texte);
	    			}else {
	    				throw new NumberFormatException();
	    			}
	    		}catch(NumberFormatException n) {
    				prop.setProperty("ht", Integer.toString(Config.getHAUTEUR()));
	    			System.out.println("Veuillez rentrer un nombre valide superieur à 0");
	    		}
	    		formatHt.setText(Integer.toString(Config.getHAUTEUR()));
	    		
	    		try {
	    			texte = formatLgr.getText();
	    			valeur = Integer.valueOf(texte);
	    			if(valeur >= 400 && valeur <= 2000) {
	    				Config.setLARGEUR(valeur);
	    				prop.setProperty("lgr", texte);
	    			}else {
	    				throw new NumberFormatException();
	    			}
	    		}catch(NumberFormatException n) {
    				prop.setProperty("lgr", Integer.toString(Config.getLARGEUR()));
	    			System.out.println("Veuillez rentrer un nombre valide superieur à 0");
	    		}
	    		formatLgr.setText(Integer.toString(Config.getLARGEUR()));
	    		
	    		try {
	    			texte = formatHtCarte.getText();
	    			valeur = Integer.valueOf(texte);
	    			if(valeur >= 10 && valeur <= 15) {
	    				Config.setHAUTEUR_CARTE(valeur);
	    				prop.setProperty("htCarte", texte);
	    			}else {
	    				throw new NumberFormatException();
	    			}
	    		}catch(NumberFormatException n) {
    				prop.setProperty("htCarte", Integer.toString(Config.getHAUTEUR_CARTE()));
	    			System.out.println("Veuillez rentrer un nombre valide superieur à 0");
	    		}
	    		formatHtCarte.setText(Integer.toString(Config.getHAUTEUR_CARTE()));
	    		
	    		try {
	    			texte = formatLgrCarte.getText();
	    			valeur = Integer.valueOf(texte);
	    			if(valeur >= 15 && valeur <= 20) {
	    				Config.setLARGEUR_CARTE(valeur);
	    				prop.setProperty("lgrCarte", texte);
	    			}else {
	    				throw new NumberFormatException();
	    			}
	    		}catch(NumberFormatException n) {
    				prop.setProperty("lgrCarte", Integer.toString(Config.getLARGEUR_CARTE()));
	    			System.out.println("Veuillez rentrer un nombre valide superieur à 0");
	    		}
	    		formatLgrCarte.setText(Integer.toString(Config.getLARGEUR_CARTE()));
	    		
	    		try {
	    			texte = formatHeros.getText();
	    			valeur = Integer.valueOf(texte);
	    			if(valeur >= 5 && valeur <= 20) {
	    				Config.setNB_HEROS(valeur);
	    				prop.setProperty("heros", texte);
	    			}else {
	    				throw new NumberFormatException();
	    			}
	    		}catch(NumberFormatException n) {
    				prop.setProperty("heros", Integer.toString(Config.getNB_HEROS()));
	    			System.out.println("Veuillez rentrer un nombre valide entre 0 et 20");
	    		}
	    		formatHeros.setText(Integer.toString(Config.getNB_HEROS()));
	    		
	    		try {
	    			texte = formatMonstre.getText();
	    			valeur = Integer.valueOf(texte);
	    			if(valeur >= 5 && valeur <= 20) {
	    				Config.setNB_MONSTRES(valeur);
	    				prop.setProperty("mons", texte);
	    			}else {
	    				throw new NumberFormatException();
	    			}
	    		}catch(NumberFormatException n) {
    				prop.setProperty("mons", Integer.toString(Config.getNB_MONSTRES()));
	    			System.out.println("Veuillez rentrer un nombre valide entre 0 et 20");
	    		}
	    		formatMonstre.setText(Integer.toString(Config.getNB_MONSTRES()));
	    		
	    		try {
	    			texte = formatObstacle.getText();
	    			valeur = Integer.valueOf(texte);
	    			if(valeur >= 5 && valeur <= 20) {
	    				Config.setNB_OBSTACLES(valeur);
	    				prop.setProperty("obs", texte);
	    			}else {
	    				throw new NumberFormatException();
	    			}
	    		}catch(NumberFormatException n) {
    				prop.setProperty("obs", Integer.toString(Config.getNB_OBSTACLES()));
	    			System.out.println("Veuillez rentrer un nombre valide entre 0 et 20");
	    		}
	    		formatObstacle.setText(Integer.toString(Config.getNB_OBSTACLES()));
	    		
	    		Config.setetatMusique(etatMusique);
	    		prop.setProperty("musique", Boolean.toString(etatMusique));
	    		
	    		try {
	    			prop.store(new FileOutputStream("config/config.ser"), "fichier");
	    		} catch (FileNotFoundException e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    		} catch (IOException e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    		}
	    		f.setSize(Config.getLARGEUR(), Config.getHAUTEUR());
			}
		});
		op.add(valider);
		
		/*ce bouton permet de sortir des options*/
		Button sortirOp = new Button("Sortir");
		sortirOp.setPreferredSize(new Dimension (300, 150));
		sortirOp.setFont(new Font("Arial", Font.BOLD, 18));
		sortirOp.setForeground(Color.black);
		sortirOp.setBackground(Color.pink);
		sortirOp.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		cl.show(conteneur, "Menu");
			}
		});
		op.add(sortirOp);
		
		demarrer.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent event) {
				demarrer.setBackground(Color.gray);
				demarrer.setForeground(backGround);
			}
			
			public void mouseExited(MouseEvent event) {
				demarrer.setForeground(Color.black);
				demarrer.setBackground(Color.BLUE);
			}
		});
		
		sauvegarde.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent event) {
				sauvegarde.setBackground(Color.gray);
				sauvegarde.setForeground(backGround);
			}
			
			public void mouseExited(MouseEvent event) {
				sauvegarde.setBackground(Color.red);
				sauvegarde.setForeground(Color.BLACK);
			}
		});
		
		option.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent event) {
				option.setBackground(Color.gray);
				option.setForeground(backGround);
			}

			public void mouseExited(MouseEvent event) {
				option.setBackground(Color.green);
				option.setForeground(Color.BLACK);
			}
		});
		
		quitter.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent event) {
				quitter.setBackground(Color.gray);
				quitter.setForeground(backGround);
			}

			public void mouseExited(MouseEvent event) {
				quitter.setBackground(Color.pink);
				quitter.setForeground(Color.BLACK);
			}
		});
	}
	
	/**
	 * Permet de dessiner la decoration
	 * @param g Graphics
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		carte.dessinerDeco(g, this);
	}

	/**
	 * Methode qui permet de recuperer op
	 * @return JPanel
	 */
	public JPanel getOp() {
		return op;
	}

	/**
	 * Methode qui permet de modifier op
	 * @param op JPanel
	 */
	public void setOp(JPanel op) {
		this.op = op;
	}

	/**
	 * Methode qui permet de recuperer la carte
	 * @return Carte
	 */
	public Carte getCarte() {
		return carte;
	}

	/**
	 * Methode qui permet de modifier la carte
	 * @param carte Carte
	 */
	public void setCarte(Carte carte) {
		this.carte = carte;
	}
}
