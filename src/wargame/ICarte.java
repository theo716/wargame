package wargame;
import java.awt.Graphics;
/**
 * Classe de l'interface de la Carte
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public interface ICarte {
	
	/**
	 * Enum de la l'interface de la Carte
	 */
	enum emplacement {
		/**
		 * TOUT
		 */
		TOUT, 
		/**
		 * GAUCHE
		 */
		GAUCHE, 
		/**
		 * DROITE
		 */
		DROITE};
	
	/**
	 * Permet de recuperer un element
	 * @param pos Position
	 * @return Element
	 */
	Element getElement(Position pos);
	
	/**
	 * Trouve aleatoirement une position vide sur la carte
	 * @return Position
	 */
	Position trouvePositionVide();
	
	/**
	 * Trouve une position vide choisie aleatoirement parmi les 8 positions adjacentes de pos
	 * @param pos Position
	 * @return Position
	 */
	Position trouvePositionVide(Position pos);
	
	/**
	 * Permet de trouver une position vide à un emplacement
	 * @param n emplacement
	 * @return Position
	 */
	Position trouvePositionVide(emplacement n);
	/**
	 * Trouve aleatoirement un heros sur la carte
	 * @return Heros
	 */
	Heros trouveHeros(); 
	
	/**
	 * Trouve un heros sur l'une des cases autour parmi les 8 positions adjacentes de pos
	 * @param pos Position
	 * @return boolean
	 */
	boolean trouveHeros(Position pos);
	
	/**
	 * Permet de deplacer un soldat
	 * @param pos Position 
	 * @param soldat Soldat
	 */
	void deplaceSoldat(Position pos, Soldat soldat);
	
	/**
	 * Permet de faire mourir un soldat
	 * @param perso Soldat
	 */
	void mort(Soldat perso);
	
	/**
	 * Permet de faire jouer un Heros
	 * @param pos Position
	 * @param pos2 Position
	 * @return boolean
	 */
	boolean actionHeros(Position pos, Position pos2);
	
	/**
	 * Permet de faire jouer un soldat
	 * @param pj PanneauJeu
	 * @param f FenetreJeu
	 */
	void jouerSoldats(PanneauJeu pj, FenetreJeu f);
	
	/**
	 * Permet de tout dessiner
	 * @param g Graphics
	 * @param pj PanneauJeu
	 */
	void toutDessiner(Graphics g, PanneauJeu pj);
}