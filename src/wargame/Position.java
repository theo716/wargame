package wargame;

import java.io.Serializable;

/**
 * Classe de gestion des Positions
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Position implements IConfig,Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Position int de x et y pour une position
	 */
	private int x, y;
	
	/**
	 * Enum pour le SENS
	 */
	protected static enum SENS{
		/**
		 * SAM
		 */
		SAM, 
		/**
		 * ALIGNES
		 */
		ALIGNES, 
		/**
		 * SIAM
		 */
		SIAM
		}
	
	/**
	 * Constructeur d'une position
	 * @param x
	 * @param y
	 */
	Position(int x, int y) { 
		this.x = x; this.y = y; 
	}
	
	/**
	 * Contructeur d'une position
	 * @param pos
	 */
	Position(Position pos) {
		this.x = pos.x;
		this.y = pos.y;
	}
	
	/**
	 * Methode qui retourne le x d'une position
	 * @return x:int
	 */
	public int getX() { return x; }
	
	/**
	 * Methode qui retourne le y d'une position
	 * @return y:int
	 */
	public int getY() { return y; }
	
	/**
	 * Methode qui permet de modifier le x d'une position
	 * @param x int
	 */
	public void setX(int x) { this.x = x; }
	
	/**
	 * Methode qui permet de modifier le y d'une position
	 * @param y int
	 */
	public void setY(int y) { this.y = y; }
	
	/**
	 * Methode qui retourne la position d'une position
	 * @return position
	 */
	public Position getPosition() {
		return new Position(x, y);
	}
	/**
	 * Methode qui permet de savoir si une position est valide
	 * @return boolean true si valide, false sinon
	 */
	public boolean estValide() {
		if (x<0 || x>=Config.getLARGEUR_CARTE() || y<0 || y>=Config.getHAUTEUR_CARTE()) return false; else return true;
	}
	
	/**
	 * Methode toString pour une position
	 * @return String
	 */
	public String toString() { return "("+x+","+y+")"; }
	
	/**
	 * Methode qui permet de savoir si deux positions sont egales
	 * @param pos Position
	 * @return boolean
	 */
	public boolean equals(Position pos) {
		if(this.x == pos.x && this.y == pos.y) {
			return true;
		}
		return false;
	}
	
	/**
	 * Methode qui permet de donner la position voisine d'une position
	 * @param pos Position
	 * @return boolean true si est voisine sinon false
	 */
	public boolean estVoisine(Position pos) {
		return ((Math.abs(x-pos.x)<=1) && (Math.abs(y-pos.y)<=1));
	}
	
	/**
	 * Methode qui permet de calculer la distance entre deux points
	 * @param p Position
	 * @return la distance: double
	 */
	public double distance(Position p){
		double x = this.x - p.x;
		double y = this.y - p.y;
		return Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2));
	}
	
	/**
	 * Methode qui permet de connaitre le signe d'un angle
	 * @param b Position
	 * @param c Position
	 * @return SAM ALIGNES ou SIAM
	 */
	public SENS signeAngle(Position b, Position c) {
		double angle = (b.getX()-getX())*(c.getY()-getY())-(c.getX()-getX())*(b.getY()-getY());
		if(angle < 0) {
			return SENS.SAM;
		}else if(angle == 0) {
			return SENS.ALIGNES;
		}else {
			return SENS.SIAM;
		}
	}
}