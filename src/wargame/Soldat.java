package wargame;

import java.io.Serializable;

/**
 * Classe de gestion des Soldats
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Soldat extends Element implements ISoldat,Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * variable points car soldat et monstre ne donnent que des constantes
	 */
	public int points; 
	
	/**
	 * Savoir si un soldat a joue son tour
	 */
	private boolean aJoue = false;
	
	/**
	 * Utile pour l'affichage
	 */
	private boolean sonTour = false;
	
	/**
	 * Constructeur de Soldat
	 * @param type types
	 * @param pos Position
	 */
	public Soldat(types type, Position pos) {
		super(type, pos);
		aJoue = false;
		sonTour = false;
	}

	/**
	 * Methode de deplacement du soldat sur la nouvelle pos 
	 * @param newPos Position
	 */
	public void seDeplace(Position newPos) {
		this.setPos(newPos);
		aJoue = true;
	}
	
	/**
	 * Methode qui retourne le tableau de position possible à n cases, les positions sont toutes bonnes dans le tableau
	 * @param n int
	 * @return Position[]
	 */
	public Position[] positionPortee(int n) {
		if(n == 1) {
			return this.positionAutour();
		}
		else {
			Position[] position_finale = new Position[500];
			Position[] position_temporaire = new Position[500];
			Position[] position_initiale;
			int case_vide=0;
			int tmp=0;
			
			position_initiale = this.positionAutour();
			for(int i=0;i<position_initiale.length;i++) {
				position_finale[case_vide] = position_initiale[i];
				case_vide++;
			}
			
			

			for(int i=1;i<n;i++) {
				tmp = case_vide;
				for(int j=0;j<tmp;j++) {
					Soldat soldat_temporaire = new Soldat(types.Hero,position_finale[j]);
					position_temporaire = soldat_temporaire.positionAutour();
					for(Position x : position_temporaire) {
						if(x != null) {
							if(!contient(position_finale,x,case_vide)) {
								position_finale[case_vide] = x;
								case_vide++;
							}
						}
					}
					soldat_temporaire = null;
				}
			}
			return position_finale;
		}
	}
	
	/**
	 * Methode qui permet de verifier si une position est dans un tableau
	 * @param lx Position[]
	 * @param x Position
	 * @param indice int
	 * @return boolean: si contient true sinon false
	 */
	public boolean contient(Position[]lx,Position x,int indice) {
		for(int i=0;i<indice;i++) {
				if(lx[i].equals(x)) {
					return true;
			}
		}
		return false;
	}
	
	/**
	 * Methode qui envoie la liste des positions autour du soldat dans le format hexagonal
	 * @return Position[]
	 */
	public Position[] positionAutour() {
		int i, j=0;
		int x = this.getPos().getX(), y = this.getPos().getY();
		Position pos[] = new Position[6], pos2[];
		pos[0] = new Position(x-1, y);
		pos[1] = new Position(x+1, y);
		if(x % 2 == 0) {
			//System.out.println("0");
			pos[2] = new Position(x+1, y-1);
			pos[3] = new Position(x, y-1);
			pos[4] = new Position(x-1, y-1);
			pos[5] = new Position(x, y+1);
		}else {
			pos[2] = new Position(x, y-1);
			pos[3] = new Position(x+1, y+1);
			pos[4] = new Position(x, y+1);
			pos[5] = new Position(x-1, y+1);
		}
		for(i=0;i<pos.length;i++) {
			if(!pos[i].estValide()) {
				pos[i] = null;
				j++;
			}
		}
		if(j==0) return pos;
		pos2 = new Position[6-j];
		j=0;
		for(i=0;i<pos.length;i++) {
			if(pos[i] != null) {
				pos2[j] = pos[i];
				j++;
			}
		}
		return pos2;
	}
	
	/**
	 * Methode qui renvoie la liste des positions autour du soldat dans le format hexagonal
	 * @param tab Position[]
	 * @return Position[]
	 */
	public Position[] positionAutour(Position[] tab) {
		int i, j, k, l, m;
		boolean appartient = false;
		if(tab.length == 1) {
			j = 6;
			k = 1;
		}else {
			j = 2*tab.length + 6;
			k = (tab.length-6)/2;
		}
		m = tab.length;
		Soldat s;
		Position[] pos = new Position[6];
		Position[] pos2 = new Position[j];
		for(i=0;i<tab.length;i++) {
			pos2[i] = tab[i];
		}
		for(i=k;i<tab.length;i++) {
			s = new Soldat(types.Hero, tab[i]);
			pos = s.positionAutour();
			for(l=0;l<pos.length;l++) {
				for(Position p:pos2) {
					if(p.equals(pos[l])) {
						appartient = true;
						break;
					}
				}
				if(!appartient) {
				pos2[m] = pos[l];
				m++;
				}
				appartient = false;
			}
			
		}
		return tab;
	}
	
	/**
	 * Methode qui permet de connaitre la categorie du soldat
	 * @return HUMAIN NAIN ELF ou HOBBIT: String
	 */
	public String getNomType() {
		switch(this.getPoints()) {
			case 40:
				return "HUMAIN";
			case 80:
				return "NAIN";
			case 70:
				return "ELF";
			case 20:
				return "HOBBIT";
		}
		return "SOLDAT";
	}
	
	/**
	 * Methode qui permet de savoir les points du soldat
	 * @return int
	 */
	public int getPoints() {
    	return this.points;
    }
    
	/**
	 * Methode qui permet de mettre a jour les points du soldat
	 * @param pt int
	 */
	public void setPoints(int pt) {
    	this.points = pt;
    }
	
	/**
	 * Methode qui retourne la portee du soldat
	 * @return portee:int
	 */
    public int getPortee() {
    	return this.getPortee();
    }
    
    /**
     * Methode qui retourne la puissance du soldat
     * @return puissance:int
     */
    public int getPuissance() {
    	return this.getPuissance();
    }
    
    /**
     * Methode qui retourne le tir du soldat
     * @return tir:int
     */
    public int getTir() {
    	return this.getTir();
    }
	
    /**
     * Methode qui met à jour le statut du soldat
     * @param aJoue boolean
     */
    public void setAJoue(boolean aJoue) {
    	this.aJoue = aJoue;
    }
    
    /**
     * Methode qui retourne le statut du soldat
     * @return boolean
     */
    public boolean getAJoue() {
    	return aJoue;
    }
    
    /**
     * Permet de tester s'il y a un obstacle entre les deux soldats
     * @param pos Position
     * @param c Carte
     * @return boolean
     */
    public boolean testObstacle(Position pos, Carte c) {
    	int i, j;
    	Element elem;
    	Position[] tab = positionAutour(), tab2;
    	
    	for(i=0;i<tab.length;i++) {
    		if((elem = c.getElement(tab[i])) != null && elem.getType() == types.Obstacle) {
    			continue;
    		}
    		tab2 = (new Soldat(types.Hero, new Position(tab[i].getX(), tab[i].getY()))).positionAutour();
    		for(j=0;j<tab2.length;j++) {
    			if(pos.equals(tab2[j])) {
    				return false;
    			}
    		}
    	}
    	return true;
    }
    
	/**
	 * Methode de combat du soldat
	 * @param soldat Soldat
	 * @param c Carte
	 */
	public void combat(Soldat soldat, Carte c) {
		int p,i;
		int ps1,ps2;
		boolean testContact = false,testDistance = false;
			Position pos[] = this.positionAutour();
			
			for(i=0;i<pos.length;i++) {//verifie si les 2 soldats sont corps à crops
				if(pos[i].equals(soldat.getPos())) {
					testContact = true;
					break;
				}
			}
			if(testContact){//les deux soldats sont adjacents
				p = (int)(Math.random() * (this.getPuissance())+1);//puissance du coup porte (int)( 1 + (Math.random() * (4 - 1)));
			}else{
				p = this.getTir();
				/*if(getPortee()==2 && testObstacle(soldat.getPos(), c)) {
					return;
				}*/
				pos = positionPortee(this.getTir());
				for(Position pos2:pos) {
					if(pos2 != null) {
						if(pos2.equals(soldat.getPos())) {
							testDistance = true;
						}
					}
				}
				if(testDistance == false) {
					return;
				}
				
			}
			PanneauJeu.textArea.append("PUISSANCE = "+p+" ");
		//met a jour les Points de soldat1
			ps1 = this.points;
			ps2 = soldat.points;
			PanneauJeu.textArea.append("| PV avant : "+(int)soldat.points+" ");
			if(ps2 < p) {/*pour ne pas avoir de pv negatif*/
				p = ps2;
			}
			soldat.setPoints(ps2-p);
			PanneauJeu.textArea.append("| PV après : "+(int)(soldat.points) + "\n");
			if(this.points <=0) {
				c.mort(this);
			}
			if(soldat.points<=0) {
				c.mort(soldat);
			}
			
			this.aJoue = true;
			
			if(soldat.points==0) {
				return;
			}
			
			if(testContact) {
				p = (int)(Math.random() * (soldat.getPuissance())+1);
			}else {
				testDistance = false;
				p = soldat.getTir();
				pos = positionPortee(soldat.getTir());
				for(Position pos2:pos) {
					if(pos2 != null) {
						if(pos2.equals(soldat.getPos())) {
							testDistance = true;
							break;
						}
					}
				}
				if(testDistance == false) {
					return;
				}
			}
			if(ps1 < p) {/*pour ne pas avoir de pv negatif*/
				p = ps1;
			}
			PanneauJeu.textArea.append("Riposte.. \nPUISSANCE = "+ p + " " + "| PV avant "+(int)this.points +" ");
			setPoints(ps1-p);
			PanneauJeu.textArea.append("| PV apres : "+(int)(this.points)+"\n");
			if(this.points <=0) {
				c.mort(this);
			}
			if(soldat.points <=0) {
				c.mort(soldat);
			}
	}

	/**
	 * Methode qui permet de savoir si c'est le tour du soldat
	 * @return boolean
	 */
	public boolean isSonTour() {
		return sonTour;
	}

	/**
	 * Methode qui permet de mettre à jour le tour du soldat
	 * @param sonTour boolean
	 */
	public void setSonTour(boolean sonTour) {
		this.sonTour = sonTour;
	}

	@Override
	public int getTour() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void joueTour(int tour) {
		// TODO Auto-generated method stub
		
	}

}
