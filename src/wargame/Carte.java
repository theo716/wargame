package wargame;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * Classe de gestion de la Carte
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Carte implements ICarte, IConfig, Serializable {
	
	private static final long serialVersionUID = 1L;
	/** 
	 * elem: contient les differents elements presents sur la carte
	 */
	private Element[] elem = new Element[Config.getNB_ELEMENTS()];
	/**
	 * type: contient le type d'element present sur chaque case
	 */
	private types type[][] = new types[Config.getLARGEUR_CARTE()][Config.getHAUTEUR_CARTE()];
	/**
	 * visible: indique si les cases sont inconnues, explorees ou visible
	 */
	private visibilite visible[][] = new visibilite[Config.getLARGEUR_CARTE()][Config.getHAUTEUR_CARTE()];
	/**
	 * affichageHeros: nombre heros
	 */
	private int affichageHeros = Config.getNB_HEROS();
	/**
	 * affichageHeros: nombre monstres
	 */
	private int affichageMonstres = Config.getNB_MONSTRES();
	/**
	 * save: etat de la sauvegarde
	 */
	private boolean save = false;
	/**
	 * intSave: 0
	 */
	private int intSave = 0;
	/**
	 * LGR_CARTE: longueur carte
	 */
	private  int LGR_CARTE;
	/**
	 * HT_CARTE: hauteur carte
	 */
	private  int HT_CARTE;
	/**
	 * NB_HEROS: nombre heros
	 */
	private  int NB_HEROS;
	/**
	 * NB_MONS: nombre monstres
	 */
	private  int NB_MONS;
	/**
	 * NB_OBS: nombre obstacles
	 */
	private  int NB_OBS;
	/**
	 * startTime: debut du temps
	 */
	private Date startTime;
	/**
	 * endTime: fin du temps
	 */
    private Date endTime;
    /**
	 * duration: duree totale
	 */
    private long duration;
    
    
	/**
	 * Constructeur de la Carte
	 */
	public Carte() {
		int i, j;
		Position[] p;
		Position pos;
		
		LGR_CARTE = Config.getLARGEUR_CARTE();
		HT_CARTE = Config.getHAUTEUR_CARTE();
		NB_HEROS = Config.getNB_HEROS();
		NB_MONS = Config.getNB_MONSTRES();
		NB_OBS = Config.getNB_OBSTACLES();
		/*initialisation de la carte*/
		for(i=0;i<Config.getLARGEUR_CARTE();i++) {
			for(j=0;j<Config.getHAUTEUR_CARTE();j++) {
				type[i][j] = types.Vide;
				visible[i][j] = visibilite.Inconnu;
			}
		}
		
		/**
		 * Creation des Heros
		 * */
		for(i=0;i<Config.getNB_HEROS();i++) {
			pos = this.trouvePositionVide(emplacement.GAUCHE);
			elem[i] = new Heros(pos);
			Soldat s = (Soldat)elem[i];
			setCarte(pos, elem[i]);
			visible[pos.getX()][pos.getY()] = visibilite.Visible;
			p = s.positionPortee(s.getPortee());
			for(Position pt : p) {
				if(pt != null) {
					visible[pt.getX()][pt.getY()] = visibilite.Visible;
				}
			}
			
		}
		
		/**
		 * Creation des Monstres
		 * */
		for(i=Config.getNB_HEROS();i<Config.getNB_SOLDATS();i++) {
			pos = this.trouvePositionVide(emplacement.DROITE);
			elem[i] = new Monstre(pos);
			setCarte(pos, elem[i]);
			
		}
		
		/**
		 *  Creation des Obstacles
		 *  */
		for(i=Config.getNB_SOLDATS();i<Config.getNB_ELEMENTS();i++) {
			pos = this.trouvePositionVide(emplacement.TOUT);
			elem[i] = new Obstacle(pos);
			setCarte(pos, elem[i]);
		}
	}
	
	/**
	 * pour la decoration dans le menu
	 * */
	Carte(int decoration){
		int i, j;
		Position pos;
		LGR_CARTE = HT_CARTE = NB_HEROS = NB_MONS = NB_OBS = 0;
		elem = new Element[DECORATION];
		type = new types[DECORATION][DECORATION];
		visible = new visibilite[DECORATION][DECORATION];
		
		for(i=0;i<DECORATION;i++) {
			for(j=0;j<DECORATION;j++) {
				type[i][j] = types.Vide;
				visible[i][j] = visibilite.Visible;
			}
		}
		
		for(i=0;i<DECORATION;i++) {
			pos = this.trouvePosition();
			elem[i] = new Heros(pos);
			setCarte(pos, elem[i]);
		}
	}
	

	/**
	 * Methode qui permet de trouver une position vide sur la carte
	 * @return un type position(x,y) de la case vide
	 */
	private Position trouvePosition(){
		int x, y;
		do {
		x = (int)(Math.random()*5);
		y = (int)(Math.random()*5);
		}while(x == 4 && y % 2 == 1);
		return new Position(x, y);
	}
	
	/**
	 * Methode qui permet de retourner le tableau types de carte
	 * @return types[][]
	 */
	public types[][] retourneType(){
		return this.type;
	}
	
	/**
	 * Methode qui permet de retourner le tableau visibilite de carte
	 * @return visibilite[][]
	 */
	public visibilite[][] getVisible() {
		return visible;
	}
	
	/**
	 * Methode qui permet de retourner le tableau Element de carte
	 * @return Element[]
	 */
	public Element[] retournElem() {
		return this.elem;
	}
	
	/**
	 * Affichage de la carte dans le terminal
	 */
	public void afficherCarte() {
		System.out.println(Arrays.deepToString(this.type));
		System.out.println(Arrays.deepToString(this.elem));
	}
	
	/**
	 * Methode qui permet de mettre un element à une position sur la carte
	 * @param pos position
	 * @param elem element
	 */
	public void setCarte(Position pos, Element elem) {
		type[pos.getX()][pos.getY()] = elem.getType();
	}
	
	/**
	 * Methode qui permet de deplacer un soldat sur la carte !
	 * @param pos une position
	 * @param soldat un soldat à deplacer
	 */
	public void deplaceSoldat(Position pos, Soldat soldat) {
		Position[] posSoldat;
		type[soldat.getPos().getX()][soldat.getPos().getY()] = types.Vide;
		type[pos.getX()][pos.getY()] = soldat.getType();
		soldat.seDeplace(pos);
		soldat.setAJoue(true);
		posSoldat = soldat.positionAutour();
		setVisible(posSoldat);
	}
	
	/**
	 * Methode qui permet de deplacer un monstre sur la carte !
	 * @param pos une position
	 * @param soldat un monstre a deplacer
	 */
	public void deplaceMonstre(Position pos, Soldat soldat) {
		type[soldat.getPos().getX()][soldat.getPos().getY()] = types.Vide;
		type[pos.getX()][pos.getY()] = soldat.getType();
		soldat.seDeplace(pos);
		soldat.setAJoue(true);
	}
	
	/**
	 * Methode qui permet de mettre une pos visible sur la carte
	 * @param pos une position
	 */
	public void setVisible(Position[] pos) {
		for(Position p : pos){
			visible[p.getX()][p.getY()] = visibilite.Visible;
		}
	}
	
	/**
	 * Methode qui permet de recuperer un element de carte à une certaine position
	 * @param pos une position
	 */
	public Element getElement(Position pos) {
		int i;
		for(i=0;i<Config.getNB_ELEMENTS();i++) {
			if(this.elem[i].getPos().equals(pos)) {
				return elem[i];
			}
		}
		return null;
	}

	public Position trouvePositionVide() {
		return null;
	}

	public Position trouvePositionVide(Position pos) {
		return null;
	}
	
	/**
	 * Methode recherche une case vide en fonciton des restrictions d'emplacement et renvoie la position
	 * @return une position
	 * */
	public Position trouvePositionVide(ICarte.emplacement n) {
		int x, y;
		
		/*dans ce cas on cherche une case vide car il n'y a pas de restriction*/
		if(n == emplacement.TOUT) {
			do {
				x = (int)(Math.random()*Config.getLARGEUR_CARTE());
				y = (int)(Math.random()*Config.getHAUTEUR_CARTE());
				if(x == Config.getLARGEUR_CARTE()-1 && y % 2 == 1) {
					continue;
				}
			}while(type[x][y] != types.Vide);
			
		/*dans ce cas, l'element doit etre place sur le côte gauche de la carte*/
		}else if(n == emplacement.GAUCHE) {
			do {
				if(Config.getLARGEUR_CARTE() % 2 == 0) {
					x = (int)(Math.random()*(int)(Config.getLARGEUR_CARTE()/2));
				}else {
					x = (int)(Math.random()*Config.getLARGEUR_CARTE()/2);
				}
				y = (int)(Math.random()*Config.getHAUTEUR_CARTE());
			}while(type[x][y] != types.Vide);
			
			/*dans ce cas, l'element doit etre place sur le côte droit de la carte*/
		}else {
			do {
				if(Config.getLARGEUR_CARTE() % 2 == 0) {
					x = (int)(Math.random()*(int)(Config.getLARGEUR_CARTE()/2))+(int)(Config.getLARGEUR_CARTE()/2);
				}else {
					x = (int)(Math.random()*Config.getLARGEUR_CARTE()/2)+(int)(Config.getLARGEUR_CARTE()/2);
				}
				y = (int)(Math.random()*Config.getHAUTEUR_CARTE());
				if(x == Config.getLARGEUR_CARTE()-1 && y % 2 == 1) {
					continue;
				}
			}while(type[x][y] != types.Vide);
		}
		return new Position(x, y);
	}
	
	public Heros trouveHeros() {
		return null;
	}

	/**
	 * Methode qui indique s'il y a un hero sur l'une des cases autour
	 * @param pos une position
	 * @return Boolean
	 */
	public boolean trouveHeros(Position pos) {
		Position[] p = (new Soldat(types.Hero, pos)).positionAutour();
		for(Position pt : p) {
			if(getElement(pt) == new Heros(pos)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Mort d'un soldat sur la carte
	 * @param perso le soldat qui est mort
	 */
	public void mort(Soldat perso) {
		PanneauJeu.textArea.append(perso.getNomType() + " est mort !\n");
		
		if(type[perso.getPos().getX()][perso.getPos().getY()] == types.Hero) {
			setCompteurHeros(affichageHeros-1);
		}else if(type[perso.getPos().getX()][perso.getPos().getY()] == types.Monstre) {
			setCompteurMonstre(affichageMonstres-1);
		}
		PanneauJeu.infos_jeu.setText("Nombre Monstres en vie : "+ affichageMonstres +" Nombre Heros en vie : "+ affichageHeros);
		
		Position[] p;
		type[perso.getPos().getX()][perso.getPos().getY()] = types.Vide;
		p = Arrays.copyOf(perso.positionAutour(), (perso.positionAutour()).length+1);
		p[p.length-1] = perso.getPos();
		p[0] = new Position(-10, -10);
		perso.estMort();
	}
	
	/** 
	 * Partie de code d'action heros pour verifier la position 
	 * @param pos position1
	 * @param pos2 position2
	 * @return boolean
	 * */
	public boolean verificationPosition(Position pos, Position pos2) {
		int i, autour = 0;
		Soldat soldat1;
		Position[] tabPos = new Position[6];
		Position[] tabPosn;
		boolean autourn = false;
		
		/*on verifie que pos2 est valide*/
		if(!pos2.estValide()) {
			return false;
		}
		if(type[pos.getX()][pos.getY()] != types.Hero || (type[pos2.getX()][pos2.getY()] == types.Hero && !pos.equals(pos2))|| type[pos2.getX()][pos2.getY()] == types.Obstacle /*heros deja jouer son tour|| si pos2 n'est pas adjacente*/ ) {
			return false;
		}
		
		/*on verifie que pos2 est autour du soldat*/
		soldat1 = (Soldat)getElement(pos);

		tabPosn = soldat1.positionPortee(soldat1.getTir());
		if(pos.equals(pos2)) {
			autourn = true;
		}else {
			for(i=0;i<tabPosn.length;i++) {
				if(tabPosn[i] !=null) {
					if(pos2.equals(tabPosn[i])) {
						autourn = true;
						break;
					}
				}
			}
		}
		
		if(autourn == false) {
			return false;
		}
		
		tabPos = soldat1.positionAutour();
		for(i=0;i<tabPos.length;i++) {
			if(pos2.equals(tabPos[i])) {
				autour = 1;
				break;
			}
		}
		if(autour == 0 && type[pos2.getX()][pos2.getY()] == types.Vide) {
			return false;
		}
		return true;
	}
	
	/**
	 * Methode qui gere le tour d'un hero
	 * @param pos position1
	 * @param pos2 position2
	 */
	public boolean actionHeros(Position pos, Position pos2) {
		Soldat soldat1, soldat2;
		
		/*on verifie que pos2 est autour du soldat*/
		soldat1 = (Soldat)getElement(pos);
		soldat2 = (Soldat)getElement(pos2);	
		
		/*cas de la regeneration*/
		if(pos.equals(pos2)) {
			if(soldat1.points < soldat1.getPoints()) {
				PanneauJeu.textArea.append("Regeneration de "+soldat1.getNomType()+ " ");
				int random = (int)(Math.random() * (10)+1);/*regenre un nombre entre 1 et 10*/
				if(soldat1.points+random>soldat1.getPoints()) {//pour ne pas avoir de depacement da pv max
					soldat1.setPoints(soldat1.getPoints());
				}else {
					soldat1.setPoints(soldat1.points + random);
				}
				PanneauJeu.textArea.append("+"+random+" PV\n");
			}else {
				PanneauJeu.textArea.append("Repos.. qui mène à rien.. \n");
			}
			return true;
		}
		
		/*cas du deplacement*/
		
		/*si c'est vide le heros se deplace a cette pos*/
		if(type[pos2.getX()][pos2.getY()] == types.Vide) {
			PanneauJeu.textArea.append("Deplacement..\n");
			deplaceSoldat(pos2, soldat1); 
		}else {
			if(type[pos2.getX()][pos2.getY()] == types.Monstre) {
			/*si il y a un monstre a pos2 le heros le combat*/
				PanneauJeu.textArea.append("Affrontement.. \n"+soldat1.getNomType()+" à la position : "+ soldat1.getPos() +" vs "+soldat2.getNomType()+" à la position : "+ soldat2.getPos() + "\n");
				soldat1.combat(soldat2, this);
			}
		}
		return true;
	}
	
	/**
	 * Methode qui gere le tour d'un monstre
	 * @param pos position de l'action
	 * @return boolean
	 */
	public boolean actionMonstre(Position pos) {
		
		/* Recupere le monstre */
		Soldat monstre = (Soldat)getElement(pos);
		
		/* position_autour_monstre est un tableau avec les point autour du monstre */
		Position[] position_autour_monstre = monstre.positionAutour();
		/* Si le monstre attaque au corps à corps */
		if(monstre.getTir() == 0) {
			
			/* Cherche autour du monstre pour attaquer */
			for(int i=0;i<position_autour_monstre.length;i++) {
				
				/* S'il y a un heros à coter de lui */
				if(type[position_autour_monstre[i].getX()][position_autour_monstre[i].getY()] == types.Hero) {
					
					Position position_a_attaquer = new Position(position_autour_monstre[i].getX(),position_autour_monstre[i].getY());
					Soldat soldat_a_attaquer = (Soldat)getElement(position_a_attaquer);
					
					PanneauJeu.textArea.append("Affrontement au corps à corps.. \n"+monstre.getNomType()+" à la position : "+ monstre.getPos() +" vs "+soldat_a_attaquer.getNomType()+" à la position : "+ soldat_a_attaquer.getPos() + "\n");
					monstre.combat(soldat_a_attaquer, this);
					return true;
				}
			}
		}else {
			/* 
			 * ici c'est le cas ou le monstre frappe à distance
			 */
			
			/* Meme distance de vision et de tir */
				Position[] aPorteeMonstre = monstre.positionPortee(monstre.getTir());
				for(int i=0;i<aPorteeMonstre.length;i++) {
					if(aPorteeMonstre[i] != null) {
						if(type[aPorteeMonstre[i].getX()][aPorteeMonstre[i].getY()] == types.Hero){
							Soldat soldat_a_attaquer = (Soldat)getElement(aPorteeMonstre[i]);
							PanneauJeu.textArea.append("Affrontement à distance.. \n"+monstre.getNomType()+" à la position : "+ monstre.getPos() +" vs "+soldat_a_attaquer.getNomType()+" à la position : "+ soldat_a_attaquer.getPos() + "\n");
							monstre.combat(soldat_a_attaquer, this);
							return true;
						}
					}
				}
			}
		/* Generation d'un nombre alea entre 0 et la taille de pos
			 * pour definir un deplacement aleatoire si le monstre a pas attaque
			 */
			int alea = 0 + (int)(Math.random()*(((position_autour_monstre.length-1) - 0) + 1));
			
			/* Si jamais la case est pas vide on retire un nombre tant que la case n'est pas vide */
			while(type[position_autour_monstre[alea].getX()][position_autour_monstre[alea].getY()] != types.Vide) {
				alea = 0 + (int)(Math.random()*(((position_autour_monstre.length-1) - 0) + 1));
			}
			
			/* Creation de la nouvelle pos pour deplacer le monstre */
			Position deplacement = new Position(position_autour_monstre[alea].getX(),position_autour_monstre[alea].getY());
			/* deplacement du monstre */
			PanneauJeu.textArea.append("Deplacement.. \n");
			deplaceMonstre(deplacement,monstre);
			
			return true;
		}
	
	/** 
	 * Fin de tour
	 */
	public void finTour() {
		int i, j;
		Position[] p;
		Soldat s;
		for(i=0;i<Config.getLARGEUR_CARTE();i++) {
			for(j=0;j<Config.getHAUTEUR_CARTE();j++) {
				if(visible[i][j] == visibilite.Visible) {
					visible[i][j] = visibilite.Explore;
				}
			}
		}
		for(i=0;i<Config.getNB_HEROS();i++) {
			s = (Soldat)elem[i];
			p = s.positionPortee(s.getPortee());
			for(Position pt : p) {
				if(pt != null) {
					visible[pt.getX()][pt.getY()] = visibilite.Visible;
				}
			}
		}
	}
	
	/**
	 * Methode qui permet de mettre save à true
	 */
	public void save() {
		save = true;
	}

	/**
	 * Methode qui permet de mettre save à false
	 */
	public void restaure() {
		save = false;
	}
	
	/**
	 * Boucle principale
	 * @param pj panneaujeu
	 * @param f fenetrejeu
	 */
	public void jouerSoldats(PanneauJeu pj, FenetreJeu f) {
		if(Config.getetatMusique()) {
			Config.startMusique();
		}
		
		if(save == true) {
			Config.setLARGEUR_CARTE(LGR_CARTE);
			Config.setHAUTEUR_CARTE(HT_CARTE);
			Config.setNB_HEROS(NB_HEROS);
			Config.setNB_MONSTRES(NB_MONS);
			Config.setNB_OBSTACLES(NB_OBS);
		}
		
		//long start = System.nanoTime();    
		/* Tchat log */
		PanneauJeu.textArea.append("La partie commence ! Bonne chance ! :D\n");
		
		
		/* Initialisation */
		Soldat soldat1;
		boolean flag = !false;
		int compteurHeros = Config.getNB_HEROS() ;
		int compteurMonstre = Config.getNB_MONSTRES();
	    long duration;
	    long minutes;
	    long seconds;
		
		int nbtours =0;
		
		this.setStartTime(new Date());
		/* Boucle de jeu */
		while(flag) {		
			nbtours++;
			PanneauJeu.textArea.append("=====================================\n");
			PanneauJeu.textArea.append("Debut du tour "+nbtours+"\n");
			PanneauJeu.textArea.append("=====================================\n");
			/* Initialisation des variables pour savoir si c'est la fin de partie*/
			compteurHeros = Config.getNB_HEROS() ;
			compteurMonstre = Config.getNB_MONSTRES();
			
			
			/* Pour chaque Heros il joue son tour */
			for(int i=0;i<Config.getNB_HEROS();i++) {	
				/*on relance le tour du bon soldat en cas de restauration d'une sauvegarde*/
				if(save == true) {
					i = intSave;
					save = false;
				}
				intSave = i;
				
				
				/* On verifie que le hero n'est pas mort sinon il joue pas */
					Element s = elem[i]; /* Recuperation du heros qui joue */
					
					/* Si le hero est mort donc en position -10 -10 */
					if(s.getPos().equals(new Position(-10, -10))) {
						
						/* On retire 1 au compteur */
						compteurHeros--;
						/* Si un des deux compteurs est à 0 alors */
						if((compteurHeros == 0) ||(compteurMonstre == 0)) {
							this.setEndTime(new Date());
						    this.setDuration( (this.getEndTime().getTime() - this.getStartTime().getTime()) - this.getDuration()); 
						    minutes = TimeUnit.MILLISECONDS.toMinutes(this.duration);
						    seconds = TimeUnit.MILLISECONDS.toSeconds(this.getDuration()) % 60;
						    
							/* Plus de monstre ou de heros = gameover */
						    PanneauJeu.textArea.append("=====================================\n");
							PanneauJeu.textArea.append("Fin de partie!\n");
							
							/* Game Over */
							flag = false;
							/* On stop la musique */
							if(Config.getetatMusique()) {
								Config.stopMusique();
							}
							
							/* On affiche un message dans le tchat pour un recapitulatif */
								PanneauJeu.textArea.append("Defaite..\n");
								PanneauJeu.textArea.append("Duree de la partie: " + minutes + " minutes " + seconds + " secondes\n");
								PanneauJeu.textArea.append("Monstre restants: " + compteurMonstre + "\n");
								
							/*creation de le fenetre de fin de jeu*/
							JFrame fenetre = new JFrame();
							fenetre.setLocationRelativeTo(null);
							
							/*dialogue entre cette fenetre et le jeu*/
							JDialog d = new JDialog(fenetre , "Defaite !", true);
							d.setLayout( new FlowLayout() );

							Button quitter = new Button("Retour au Menu");
							quitter.setSize(180, 60);
							quitter.setPreferredSize(new Dimension (300, 150));
							quitter.setForeground(Color.black);
							quitter.setBackground(Color.gray);
							quitter.setFont(new Font("Arial", Font.BOLD, 18));
							quitter.setSize(180,60);
							
							quitter.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									PanneauJeu.carte = null;
									/* On stop la musique */
									if(Config.getetatMusique()) {
										Config.stopMusique();
									}
									d.setVisible(false);
									f.getCl().show(f.getJp(), "Menu");
								}
							});

							d.add(quitter);
							d.setSize(350,300);
							d.setVisible(true);
						}
						
						/* et on passe a la boucle suivante dans le cas ou le hero est mort */
						continue;
					}
					
					
					/* Si le hero est en vie on le recupere dans une variable soldat */
					soldat1 = (Soldat)getElement(s.getPos());
					PanneauJeu.textArea.append("----------------\n");
					PanneauJeu.textArea.append(soldat1.getNomType() +  " : à la position "+ soldat1.getPos() +"\n");
					
					/* On dit que c'est ce soldat qui joue pour l'affichage */
					soldat1.setSonTour(true);	
					
					/* repaint du panneau jeu */
					pj.repaint();
					
					PanneauJeu.textArea.append("PV = "+soldat1.getPoints()+"\n");
					
					
					/* Boucle tant que le case cliquee n'est pas valide */
					boolean tmp = true;	
					while(tmp) {if(pj.getCarte() == null) {return;}
						System.out.print("");
						if(verificationPosition(s.getPos(), new Position(pj.getX_pos_action_soldat(),pj.getY_pos_action_soldat())) == true) {
							tmp = false;
						}
					}
					
					/* Si la case cliquer est bonne: 
					 * on appel la methode action heros qui choisis quel action faire en fonction de la position
					 * choisis par le joueur */
					actionHeros(s.getPos(), new Position(pj.getX_pos_action_soldat(),pj.getY_pos_action_soldat()));
					pj.setX_pos_action_soldat(-1);
					pj.setY_pos_action_soldat(-1);
					
					
					finTour();
					pj.repaint();
					soldat1.setSonTour(false);
				}
			/* Pour chaque Monstre il joue son tour */
			for(int i=Config.getNB_HEROS();i<Config.getNB_MONSTRES()+Config.getNB_HEROS();i++) {
				
				/* On verifie que le monstre n'est pas mort sinon il joue pas */
					Element s = elem[i];
					if(s.getPos().equals(new Position(-10, -10))) {
						compteurMonstre--;
						if((compteurHeros == 0) ||(compteurMonstre == 0)) {
							endTime = new Date();
						    duration = endTime.getTime() - startTime.getTime();
						    minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
						    seconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
						    
							/* Plus de monstre ou de heros = gameover */
						    PanneauJeu.textArea.append("=====================================\n");
							PanneauJeu.textArea.append("Fin de partie!\n");
							
							/* Game Over */
							flag = false;
							/* On stop la musique */
							if(Config.getetatMusique()) {
								Config.stopMusique();
							}
							
							/* On affiche un message dans le tchat pour un recapitulatif */
								PanneauJeu.textArea.append("Victoire !!\n");
								PanneauJeu.textArea.append("Duree de la partie: " + minutes + " minutes " + seconds + " secondes\n");
								PanneauJeu.textArea.append("Heros restants: " + compteurHeros + "\n");
							
							/*creation de le fenetre de fin de jeu*/
							JFrame fenetre = new JFrame();
							fenetre.setLocationRelativeTo(null);
							
							/*dialogue entre cette fenetre et le jeu*/
							JDialog d = new JDialog(fenetre , "Victoire ! ", true);
							d.setLayout( new FlowLayout() );

							Button quitter = new Button("Retour au Menu");
							quitter.setSize(180, 60);
							quitter.setPreferredSize(new Dimension (300, 150));
							quitter.setForeground(Color.black);
							quitter.setBackground(Color.gray);
							quitter.setFont(new Font("Arial", Font.BOLD, 18));
							quitter.setSize(180,60);
							
							quitter.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									PanneauJeu.carte = null;
									/* On stop la musique */
									if(Config.getetatMusique()) {
										Config.stopMusique();
									}
									d.setVisible(false);
									f.getCl().show(f.getJp(), "Menu");
								}
							});

							d.add(quitter);
							d.setSize(350,300);
							d.setVisible(true);
						}
						
						/* Si le monstre est mort on passe à l'indice suivant */
						continue;
					}
					
					Soldat s1 = (Soldat)s;
					PanneauJeu.textArea.append("----------------\n");
					PanneauJeu.textArea.append(s1.getNomType() + " : PV = " + s1.points + "\n");
					
					Random rand = new Random();
					int alea = rand.nextInt(4);
					switch(alea) {
						case 0: PanneauJeu.textArea.append("Hum.. Je reflechis..\n"); break;
						case 1: PanneauJeu.textArea.append("Que faire..\n"); break;
						case 2: PanneauJeu.textArea.append("J'ai une idee.. mouahah..\n"); break;
						case 3: PanneauJeu.textArea.append("J'arrive te chercher.. \n"); break;
						default: PanneauJeu.textArea.append("ok\n"); break;
					}
					
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					

					/* Action du monstre */
					actionMonstre(s.getPos());

					pj.repaint();
				}
		//	}
			
		}
	}

	/**
	 * Methode qui permet de dessiner
	 * @param g Graphics
	 * @param s Soldat
	 * @param x int
	 * @param y int
	 */
	public void dessinerSold(Graphics g,Soldat s, int x,int y){
		switch(s.getNomType()) {
		case "HUMAIN":
			try {
				g.drawImage(humain, x, y+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		case "NAIN":
			try {
				g.drawImage(nain, x, y+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		case "ELF":		
			try {
				g.drawImage(elf, x, y+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		case "HOBBIT":
			try {
				g.drawImage(hobbit, x, y+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		}
	}
	
	/**
	 * Methode qui permet de desiner les obstacles
	 * @param g Graphics
	 * @param obs Obstacle
	 * @param x int[]
	 * @param y int[]
	 * @param expl boolean
	 */
	public void dessinerObs(Graphics g,Obstacle obs, int[] x,int[] y,boolean expl){
		switch(obs.toString()) {
		case "ROCHER":
			if(expl) {
				g.setColor(COULEUR_ROCHER);
			}else{
				g.setColor(new Color(128, 125, 125));
			}
			g.fillPolygon(x, y,6);
			try {
				g.drawImage(rocher, x[0], y[0]+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		case "FORET":
			if(expl) {
			g.setColor(COULEUR_FORET);
			}else {
				g.setColor(new Color(22,48,35));
			}
			g.fillPolygon(x, y,6);
			try {
				g.drawImage(foret, x[0], y[0]+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		case "EAU":
			if(expl) {
			g.setColor(COULEUR_EAU);
			}else {
				g.setColor(new Color(0,19,82));
			}
			g.fillPolygon(x, y,6);
			try {
				g.drawImage(eau, x[0], y[0]+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		}	
	}
	
	/**
	 * Methode qui permet de dessiner les Monstres
	 * @param g Graphics
	 * @param mons_tmp Monstre
	 * @param x int
	 * @param y int
	 */
	public void dessinerMons(Graphics g,Monstre mons_tmp, int x,int y){
		switch(mons_tmp.getNomType()) {
		case "TROLL":
			try {
				g.drawImage(troll, x, y+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		case "ORC":
			try {
				g.drawImage(orc, x, y+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		case "GOBELIN":
			try {
				g.drawImage(gobelin, x, y+10, 30,50, null);
			}catch(Exception e) {
				System.out.println("Image introuvable");
			}
			break;
		}
			
	}
	
	/**
	 * Methode qui permet de tout dessiner
	 * @param g Graphics
	 * @param pj PanneauJeu
	 */
	public void toutDessiner(Graphics g, PanneauJeu pj) {
		int i, j;
		int[] tabX = new int[6];
		int[] tabY = new int[6];
		Position pos;
		Element s;
		Soldat sol_tmp;
		Monstre mons_tmp;
		Obstacle obs_tmp;
		boolean expl = true;
		/* g.drawImage(ocean, 0, 0, 800,800, null);*/
		for(i=0;i<Config.getHAUTEUR_CARTE();i++) {
			for(j=0;j<Config.getLARGEUR_CARTE();j++) {
				pos = pj.getPosPJ()[j][i];
				tabX = (new Hexagone(pos)).tabX();
				tabY = (new Hexagone(pos)).tabY();
				
				/*si l'emplacement est inconnu alors couleur inconnu*/
				if(visible[j][i] == visibilite.Inconnu) {
					g.setColor(Color.gray);
					g.fillPolygon(tabX, tabY, 6);
					g.drawImage(brouillard, tabX[0], tabY[0], 30,60, null);
				}else {
					/*sinon l'emplacement est soit visible, soit explore*/
					switch(type[j][i]) {
					//dans tout les cas, la case est vide
					//il y a un hero
					case Hero:
						g.setColor(COULEUR_HEROS);
						g.fillPolygon(tabX, tabY, 6);
						s= this.getElement(new Position(j,i));
						sol_tmp = (Soldat)s;
						this.dessinerSold(g,sol_tmp, tabX[0], tabY[0]);
						break;
					case Vide:
						/*si deja explore mais plus dessus*/
						if(visible[j][i] == visibilite.Explore) {
							g.setColor(COULEUR_EXPL);
							g.fillPolygon(tabX, tabY, 6);
						}else {
							g.setColor(COULEUR_VIDE);
							g.fillPolygon(tabX, tabY, 6);
						}
						break;
					case Monstre:
						//si la case est visible, on affiche le monstre, sinon on ne l'affiche pas
						if(visible[j][i] == visibilite.Visible) {
							g.setColor(COULEUR_MONSTRES);
							g.fillPolygon(tabX, tabY, 6);
							s= this.getElement(new Position(j,i));
							mons_tmp = (Monstre)s;
							this.dessinerMons(g,mons_tmp, tabX[0], tabY[0]);
						}else {
							g.setColor(COULEUR_EXPL);
							g.fillPolygon(tabX, tabY, 6);
						}
						break;
					case Obstacle:
						g.setColor(getElement(new Position(j, i)).getColor());
						g.fillPolygon(tabX, tabY, 6);
						if(visible[j][i] == visibilite.Explore) {
							expl = false;
						}else {
							expl = true;
						}
						s= this.getElement(new Position(j,i));
						obs_tmp = (Obstacle)s;
						this.dessinerObs(g,obs_tmp, tabX, tabY,expl);
					}
					
				}
				
				g.setColor(Color.black);
				g.drawPolygon(tabX, tabY, 6);
				
			}
		}
		
		/* Mettre la case du joueur en cours en couleur mauve 
		 *  et afficher les cases de deplacements */
		for(int w=0; w<Config.getNB_HEROS();w++) {
			if(elem[w] != null) {
				Soldat stmp = (Soldat)elem[w];
				if(stmp.isSonTour()) {
					pos = pj.getPosPJ()[elem[w].getPos().getX()][elem[w].getPos().getY()];
					tabX = (new Hexagone(pos)).tabX();
					tabY = (new Hexagone(pos)).tabY();

					
					g.setColor(Color.cyan);
					//g.fillOval(tabX[0], tabY[0]+15, 30, 30);
					
					Position[] posautourn = stmp.positionPortee(stmp.getTir());
					
					if(posautourn != null) {
						g.setColor(new Color(188,75,81));
						for(Position p: posautourn) {
							if(p != null) {
								p = pj.getPosPJ()[p.getX()][p.getY()];
								g.drawPolygon((new Hexagone(p)).tabX(), (new Hexagone(p)).tabY(), 6);
							}
						}
					}
					
					Position[] posautour = stmp.positionAutour();
					
					if(posautour != null) {
						g.setColor(new Color(91,142,125));
						for(Position p: posautour) {
							p = pj.getPosPJ()[p.getX()][p.getY()];
							g.drawPolygon((new Hexagone(p)).tabX(), (new Hexagone(p)).tabY(), 6);
						}
					}
					
				}
			}
		}
		
	}
	

	/**
	 * Methode qui permet de faire de la deco
	 * @param g Graphics
	 * @param pj MenuJeu
	 */
	public void dessinerDeco(Graphics g, MenuJeu pj) {
		int i, x, y, rand;
		int[] tabX = new int[6];
		int[] tabY = new int[6];
		boolean expl = true;
		Position pos;
		for(i=0;i<DECORATION;i++) {
				x = (int)(pj.getOp().getAlignmentX()+Math.random()*Config.getLARGEUR());
				y = (int)(pj.getOp().getAlignmentY()+Math.random()*Config.getHAUTEUR());
				pos = new Position(x, y);
				tabX = (new Hexagone(pos)).tabX();
				tabY = (new Hexagone(pos)).tabY();
				
				rand = (int)(Math.random()*3);
				
					switch(rand) {
					case 0:
						Heros h = new Heros(pos);
						dessinerSold(g, h, tabX[0], tabY[0]);
						break;
					case 1:
						Monstre m = new Monstre(pos);
						dessinerMons(g, m, tabX[0], tabY[0]);
						break;
					default :
						Obstacle o = new Obstacle(pos);
						dessinerObs(g, o, tabX, tabY,expl);
					}
				g.setColor(Color.black);
				g.drawPolygon(tabX, tabY, 6);
		}
	}
	
	/**
	 * Methode qui permet d'avoir le nombre de Heros
	 * @return int: le nombre de Heros
	 */
	public int getCompteurHeros() {
		return affichageHeros;
	}

	/**
	 * Methode qui permet de modifier le nombre de Heros
	 * @param affichageHeros : nombre Heros
	 */
	public void setCompteurHeros(int affichageHeros) {
		this.affichageHeros = affichageHeros;
	}

	/**
	 * Methode qui permet d'avoir le nombre de Monstre
	 * @return int: le nombre de Monstre
	 */
	public int getCompteurMonstre() {
		return affichageMonstres;
	}

	/**
	 * Methode qui permet de modifier le nombre de Monstre
	 * @param affichageMonstres : nombre Monstres
	 */
	public void setCompteurMonstre(int affichageMonstres) {
		this.affichageMonstres = affichageMonstres;
	}

	/**
	 * Methode qui permet de connaitre le temps du debut
	 * @return Date
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Methode qui permet de mofidier le temps du debut
	 * @param startTime : temps du debut
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Methode qui permet de modifier le temps de fin
	 * @return Date
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Methode qui permet de modifier le temps de fin
	 * @param endTime : temps de fin
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * Methode qui permet d'obtenir la duree
	 * @return long
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * Methode qui permet de modifier la duree
	 * @param duration : duree en cours
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}
}