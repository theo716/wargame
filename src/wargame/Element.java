package wargame;
import java.awt.Color;
import java.io.Serializable;

/**
 * Classe de gestion de les elements
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class Element implements IConfig,Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * type de l'element
	 */
	private final types type;
	
	/**
	 * Position d'un element
	 */
	private Position pos;
	
	/**
	 * Constructeur de la classe Element
	 * @param type types
	 * @param pos Position
	 */
	public Element(types type, Position pos) {
		this.type = type;
		this.setPos(pos);
	}

	/**
	 * Methode qui permet de modifier la position d'un element pour le declarer mort
	 */
	public void estMort() {
		setPos(new Position(-10, -10));
	}
	
	/**
	 * Methode qui permet de connaitre la position d'un element
	 * @return Position
	 */
	public Position getPos() {
		return pos;
	}
	
	/**
	 * Methode qui permet de connaitre le type d'un element
	 * @return types
	 */
	public types getType() {
		return type;
	}
	
	/**
	 * Methode qui permet de connaitre la couleur d'un element
	 * @return Color
	 */
	public Color getColor() {
		return this.getColor();
	}
	
	/**
	 * Methode qui peremet d'avoir les positions autour d'un element
	 * @return Position[]
	 */
	public Position[] positionAutour() {
		return this.positionAutour();
	}

	/**
	 * Methode qui permet de modifier la position d'un element
	 * @param pos Position
	 */
	public void setPos(Position pos) {
		this.pos = pos;
	}
}
