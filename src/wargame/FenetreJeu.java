package wargame;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.CardLayout;
import java.awt.Dimension;

/**
 * Classe de gestion de la Fenetre de Jeu
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class FenetreJeu extends JFrame {

	private static final long serialVersionUID = 1L;
	
	/**
	 * PanneauJeu de la fenetre
	 */
	private PanneauJeu pj;
	/**
	 * JPanel de la fenetre
	 */
	private JPanel jp;
	
	/**
	 * CardLayout de la fenetre
	 */
	private CardLayout cl;
	
	/**
	 * int pour conteneur
	 */
	int conteneur = 0;
	
	/**
	 * necessaire pour init à partir du fichier de config
	 */
	private Config conf = new Config();

	public boolean pause = false;
	
	/**
	 * Variable pour la carte
	 */
	public static Carte carte;
	
	/**
	 * Permet de creer la JFrame
	 */
	FenetreJeu(){
		/*paramètres de la JFrame*/
		setTitle("[WARGAME] KIT");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setPreferredSize(new Dimension(Config.getLARGEUR()+100, Config.getHAUTEUR()));
		setVisible(true);
		pack();
		
		/* Creation d'un JPanel qui va contenir les differents JPanel menu, PanneauJeu, ...
		 * L'utilisation de CardLayout permet d'afficher un seul JPanel parmi la liste presente dans jp
		 */
		setJp(new JPanel(new CardLayout()));
		setCl((CardLayout)getJp().getLayout());
		getContentPane().add(getJp());
		
	}
	
	/**
	 * Boucle principale du jeu
	 * @param args String[]
	 */
	public static void main(String[] args) {
		/* Creation de la fenetre de jeu */
		FenetreJeu f = new FenetreJeu();
		/*creation du jpanel menu */
		MenuJeu menu = new MenuJeu(f);
		f.getJp().add(menu, "Menu");
		
		/*ajout de l'action KeyListener au panel de jeu*/
		f.getJp().setFocusable(true);
		f.getJp().addKeyListener(new Key(f));
		
		f.getCl().show(f.getJp(), "Menu");
		menu.revalidate();
		
		while(true) {
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			switch(f.conteneur) {
			case 0:
				menu.repaint();
				break;
			case 1:
				f.getCl().show(f.getJp(), "Jeu");
				f.getPj().demarrer();
				f.conteneur = 0;
				break;
			case 2:
				f.getCl().show(f.getJp(), "Jeu");
				f.getPj().demarrer();
				f.conteneur = 0;
			}
		}
		
	}

	/**
	 * Methode qui permet de recuperer le panneaujeu
	 * @return pj le panneauJeu
	 */
	public PanneauJeu getPj() {
		return pj;
	}

	/**
	 * Methode qui permet de modifier le panneaujeu
	 * @param pj PanneauJeu
	 */
	public void setPj(PanneauJeu pj) {
		this.pj = pj;
	}

	/**
	 * Methode qui permet de recuperer le JPanel
	 * @return jp le JPanel
	 */
	public JPanel getJp() {
		return jp;
	}

	/**
	 * Methode qui permet de modifier le JPanel
	 * @param jp JPanel
	 */
	public void setJp(JPanel jp) {
		this.jp = jp;

		//f.getJp().add(menu, "Menu");
	}

	/**
	 * Methode qui permet de recuperer le CardLayout
	 * @return cl le cardLayout de la fenetre
	 */
	public CardLayout getCl() {
		return cl;
	}
	
	/**
	 * Methode qui permet de modifier le CardLayout
	 * @param cl JPanel
	 */
	public void setCl(CardLayout cl) {
		this.cl = cl;
	}
	
	/**
	 * Methode qui permet de recuperer la Carte
	 * @return carte la carte du jeu
	 */
	public Carte getCarte() {
		return carte;
	}
	
	/**
	 * Methode qui permet de restaurer la Carte en paramètre
	 * @param c Carte
	 */
	public void restaure(Carte c) {
		this.conteneur = 2;
		carte = c;
	}

	/**
	 * Methode qui permet de recuperer la config conf
	 * @return conf la configuration
	 */
	public Config getConf() {
		return conf;
	}

	/**
	 * Methode qui permet de modifier la configuration
	 * @param conf
	 */
	public void setConf(Config conf) {
		this.conf = conf;
	}
}
