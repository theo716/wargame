package wargame;

/**
 * Classe de gestion des Hexagones
 * @author Kevin
 * @version 1.0
 */
public class Hexagone implements IConfig {
	
	private final Position position[] = new Position[6];
	
	/**
	 * Constructeur d'un haxagone
	 * @param p Position
	 */
	public Hexagone(Position p){
		int pixels = NB_PIX_CASE;
		position[0] = new Position(p.getX()-pythagore(pixels)-pixels/6, p.getY()-apotheme(pixels));
		position[1] = new Position(p.getX()+pythagore(pixels)+pixels/6, p.getY()-apotheme(pixels));
		position[2] = new Position(p.getX()+pixels/2+pixels/6, p.getY());
		position[3] = new Position(p.getX()+pythagore(pixels)+pixels/6, p.getY()+apotheme(pixels));
		position[4] = new Position(p.getX()-pythagore(pixels)-pixels/6, p.getY()+apotheme(pixels));
		position[5] = new Position(p.getX()-pixels/2-pixels/6, p.getY());
	}

	/**
	 * Methode qui retourne le tableau de point de l'hexagone
	 * @return position[] est un tableau avec les points de l'hexagone
	 */
	public Position[] getPosition(){
		return position;
	}
	
	/**
	 * Methode qui retourne la position à l'index n
	 * @param n Position
	 * @return la position
	 */
	public Position getPosition(int n){
		return position[n];
	}
	
	/**
	 * Methode qui retourne le tableau de points en x de l'hexagone
	 * @return un tableau de points
	 */
	public int[] tabX() {
		int i=0;
		int[] tab = new int[6];
		for(Position p: position) {
			tab[i] = p.getX();
			i++;
		}
		return tab;
	}
	
	/**
	 * Methode qui retourne le tableau de points en y de l'hexagone
	 * @return un tableau de points
	 */
	public int[] tabY() {
		int i=0;
		int[] tab = new int[6];
		for(Position p: position) {
			tab[i] = p.getY();
			i++;
		}
		return tab;
	}
	
	/**
	 * Calcul l'apothème du point x
	 * @param x int
	 * @return la distance calculee avec l'apothème
	 */
	public int apotheme(int x) {
		return (int)Math.round(x*Math.sqrt(3)/2);
	}
	
	/**
	 * Calcul la distance au point x avec la formule de Pythagore
	 * @param x int
	 * @return la distance calculee avec Pythagore
	 */
	public int pythagore(int x) {
		return (int)Math.round(Math.sqrt(Math.pow(x/2, 2)-Math.pow(x*Math.sqrt(3)/4, 2)));
	}
	
	/**
	 * Methode qui permet de savoir si une position est dans l'hexagone
	 * @param p Position
	 * @return boolean true si le point est dedans, sinon false
	 */
	public boolean dedans(Position p) {
		int tp[] = new int[3];
		tp = troisPositionLesPlusProches(p);
		Triangle t1 = new Triangle(getPosition(tp[0]), getPosition(tp[1]), p);
		Triangle t2 = new Triangle(p, getPosition(tp[1]), getPosition(tp[2]));
		if(!t1.estConvexe() || !t2.estConvexe()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Methode qui permet d'avoir les 3 positions de l'hexagone les plus proches du point
	 * @param p
	 * @return un tableau de int qui contient les 3 positions de l'hexagone les plus proches de p
	 */
	private int[] troisPositionLesPlusProches(Position p){
		int i, j, k = 0, k_avant, k_apres, k_tmp1, k_tmp2;
		double distance[] = new double[6], distance_min[] = new double[3];
		int retour[] = new int[3];
		
		/*prendre les 3 Positions avec les distances les plus petites*/
		/*prendre celui avec la plus petite puis les deux autour du plus petit puis les 2 autours des deux plus petits*/
		
		/*recherche du Position le plus proche*/
		for(i=0;i<6;i++) {
			distance[i] = position[i].distance(p);
		}
		distance_min[0] = distance[0];
		for(j=1;j<6;j++) {
			if(distance[j] < distance_min[0]) {
				distance_min[0] = distance[j];
				k = j;
			}
		}
		/*puis chercher les 2 autres Positions*/
		k_avant = k-1;
		k_apres = k+1;
		if(k_avant < 0) {
			k_avant += 6;
		}
		if(k_apres == 6) {
			k_apres = 0;
		}
		
		if(distance[k_avant] <= distance[k_apres]) {
			distance_min[1] = distance_min[0];
			distance_min[0] = distance[k_avant];
			k_avant -= 1;
			if(k_avant < 0) {
				k_avant += 6;
			}
		}else {
			distance_min[1] = distance[k_apres];
			k_apres += 1;
			if(k_apres == 6) {
				k_apres -= 6;
			}
		}
		
		/*puis chercher le dernier Position*/
		if(distance[k_avant] <= distance[k_apres]) {
			/*l'ordre est k_avant, k_tmp1, k_tmp2*/
			k_tmp1 = k_avant+1;
			k_tmp2 = k_tmp1+1;
			if(k_tmp1 >= 6) {
				k_tmp1 -= 6;
			}
			if(k_tmp2 >= 6) {
				k_tmp2 -= 6;
			}
			retour[0] = k_avant;
			retour[1] = k_tmp1;
			retour[2] = k_tmp2;
		}else {
			/*l'ordre est k_tmp2, k_tmp1, k_apres*/
			k_tmp1 = k_apres-1;
			k_tmp2 = k_tmp1-1;
			if(k_tmp1 < 0) {
				k_tmp1 += 6;
			}
			if(k_tmp2 < 0) {
				k_tmp2 += 6;
			}
			retour[0] = k_tmp2;
			retour[1] = k_tmp1;
			retour[2] = k_apres;
		}
		return retour;
	}
}