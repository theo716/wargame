package wargame;
import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.text.DefaultCaret;

/**
 * Classe de gestion du Panneau Jeu
 * @author Kevin, Ibrahim, Theo
 * @version 1.0
 */
public class PanneauJeu extends JPanel implements IConfig,Serializable{
	
	private static final long serialVersionUID = 1L;
	/**
	 * carte : contient la carte du jeu
	 */
	public static Carte carte;
	
	/**
	 * posPJ : contient les positions sur le jpanel des differents hexagones du jeu
	 */
	private final Position[][] posPJ = new Position[Config.getLARGEUR_CARTE()][Config.getHAUTEUR_CARTE()];
	
	/**
	 * position_tour : connaitre la position du tour ou l'utilisateur clique
	 */
	private Position position_tour;
	/**
	 * pos_soldat : boolean de verification
	 */
	private boolean pos_soldat;
	/**
	 * x_pos_action_soldat : coord x de la position ou l'utilisateur clique
	 */
	private int x_pos_action_soldat = -1;
	/**
	 * y_pos_action_soldat : coord y de la position ou l'utilisateur clique
	 */
	private int y_pos_action_soldat = -1;
	/**
	 * textArea : permet d'afficher le tchat log
	 */
	public static JTextArea textArea;
	/**
	 * areaScrollPane : permet de gerer le tchat log
	 */
	public JScrollPane areaScrollPane;
	/**
	 * infos_jeu : afficher les informations du jeu
	 */
	public static JLabel infos_jeu;
	/**
	 * fi : fenetreejeu
	 */
	private FenetreJeu fi;

	/**
	 * Constructeur de la classe PanneauJeu
	 * @param f FenetreJeu
	 * @param c Carte
	 */
	public PanneauJeu(FenetreJeu f, Carte c) {
		fi = f;
		setLayout(new BorderLayout());
		/*creation de la carte*/
		if(c == null) {
			carte = new Carte();
		}else {
			carte = c;
		}
		/* Tchat Log */
		textArea = new JTextArea(10, 25);	
		
		/* Permet d'autoscroll le tchat log */
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		/* Paramètre pour le tchat log */
		textArea.setEditable(false);
		textArea.setFont(new Font("Serif", Font.BOLD, 14));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		areaScrollPane = new JScrollPane(textArea); 
		
		add(areaScrollPane, BorderLayout.EAST);
		
		JToolBar Barre = new JToolBar();
		
		
		infos_jeu = new JLabel("Nombre Monstres en vie : "+Config.getNB_MONSTRES()+" Nombre Heros en vie : "+Config.getNB_HEROS());
		infos_jeu.setForeground(Color.black);
		
		JLabel infos = new JLabel(" ");
		infos.setForeground(Color.black);

		
		JPanel SouthBar = new JPanel(new GridLayout(1,0));
		SouthBar.setPreferredSize(new Dimension(Config.getLARGEUR(),50));
		SouthBar.setBackground(Color.red);
		SouthBar.add(infos_jeu);
		SouthBar.add(infos);
		
		
		add(SouthBar,BorderLayout.SOUTH);
		
		
		/*remplissage du tableau posPJ*/
		int i, j;
		int x, y = 2*NB_PIX_CASE;
		for(i=0;i<Config.getHAUTEUR_CARTE()*2;i++) {
			x = 2*NB_PIX_CASE;
			if(i % 2 == 1) {
				x += NB_PIX_CASE*1.5/2+NB_PIX_CASE/3;
			}
			for(j=0;j<Config.getLARGEUR_CARTE();j++) {
				if((j == Config.getLARGEUR_CARTE()-1) && (Config.getLARGEUR_CARTE() % 2 == 1) && (i % 2 == 1)) {
					break;
				}
				if((i%2==0 && j%2==1) || (i%2==1 && j%2==0)) {
					continue;
				}
				getPosPJ()[j][(int)i/2] = new Position(x, y);
				x += NB_PIX_CASE*1.5+2*NB_PIX_CASE/3;
			}
			y += apotheme(NB_PIX_CASE);
		}
		
		/* Evenement quand on clique sur une case du panneau */
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Position pos2 = null;
				int i=0, j=0;
				f:
				for(Position[] p2 : getPosPJ()) {
					for(Position p3 : p2) {
						if((new Hexagone(p3)).dedans(new Position(e.getX(), e.getY()))){
							pos2 = p3;
							break f;
						}
						j++;
					}
					i++;
					j = 0;
				}
				if(pos2 != null) {
					position_tour = new Position(i,j); /* Position que l'utilisateur à cliquer pendant son tour */
					pos_soldat = true;
					x_pos_action_soldat =i;
					y_pos_action_soldat =j;
					repaint();
				}
					
			}
			
			
		});	
		
		
		this.addMouseMotionListener( new MouseAdapter () {	
			public void mouseMoved(MouseEvent e) {
				Soldat sol_tmp;
				Monstre mos_tmp;
				Obstacle ob_tmp;
				String text =" ";
				int i=0, j=0;
				f:
				for(Position[] p2 : getPosPJ()) {
					for(Position p3 : p2) {
						if((new Hexagone(p3)).dedans(new Position(e.getX(), e.getY()))){
							break f;
						}
						j++;
					}
					i++;
					j = 0;
				}
				
				if(i<Config.getLARGEUR_CARTE() && j<Config.getHAUTEUR_CARTE()) {
					Element s;
					types type[][] = carte.retourneType();
					switch(type[i][j]) {/*Lorsque l'on survole une case son type est ecrit*/
						case Hero :
							s= carte.getElement(new Position(i,j));
							sol_tmp = (Soldat)s;
							text="POS: ("+i+","+j+") "+sol_tmp.getNomType()+" "+sol_tmp.points+"PV/"+sol_tmp.getPoints();
							infos.setText(text);
							break;
						case Monstre:
							if(carte.getVisible()[i][j] == visibilite.Visible) {/*seulement si le monstres est visible*/
								s= carte.getElement(new Position(i,j));
								mos_tmp = (Monstre)s;
								text="POS: ("+i+","+j+") "+mos_tmp.getNomType()+" "+mos_tmp.getPoints()+" PV/"+mos_tmp.getPointsMax();
								infos.setText(text);
							}
							break;
						case Obstacle:
							if(carte.getVisible()[i][j] == visibilite.Visible) {/*seulement si l'obstacle est visible*/
								s= carte.getElement(new Position(i,j));
								ob_tmp = (Obstacle)s;
								text="POS: ("+i+","+j+") "+ob_tmp.toString();
								infos.setText(text);
							}
							break;
						default:
							text=" ";
							infos.setText(text);
							break;
					}
				}
			}
		});
		
		add(Barre,BorderLayout.NORTH);
		
	}
	
	/**
	 * Methode qui dessine
	 * @param g Graphics
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		carte.toutDessiner(g, this);
		
		/* Quand on clique sur la carte, sa selectionne l'hexagone sur la map et retourne la position en position_tour*/
		/* pos_soldat est un booleen */
		if(pos_soldat) {
			position_tour = getPosPJ()[position_tour.getX()][position_tour.getY()];
			pos_soldat = false;
		}
	}
	
	/**
	 * Methode apotheme
	 * @param x int
	 * @return int
	 */
	public int apotheme(int x) {
		return (int)Math.round(x*Math.sqrt(3)/2);
	}
	
	/**
	 * Methode pythagore
	 * @param x int
	 * @return int 
	 */
	public int pythagore(int x) {
		return (int)Math.round(Math.sqrt(Math.pow(x/2, 2)-Math.pow(x*Math.sqrt(3)/4, 2)));
	}
	
	/**
	 * Methode utile pour le demarrage du jeu
	 */
	public void demarrer() {
		carte.jouerSoldats(this,this.fi);
	}
	
	/**
	 * Methode pour le demarrage d'une sauvegarde
	 * @param c Carte
	 */
	public void demarrer(Carte c) {
		carte = c;
		carte.jouerSoldats(this,fi);
	}
	
	/**
	 * Methode paint
	 * @param g Graphics
	 * @param ex Hexagone
	 */
	public void paint(Graphics g,Hexagone ex) {
		g.setColor(Color.orange);
		g.fillPolygon(ex.tabX(), ex.tabY(), 6);
		g.setColor(Color.GRAY);
		g.drawPolygon(ex.tabX(), ex.tabY(), 6);
	}

	/**
	 * Methode pour savoir le boolean de verification de la position
	 * @return boolean
	 */
	public boolean isPos_soldat() {
		return pos_soldat;
	}

	/**
	 * Methode pour modifier le boolean de verification de la position
	 * @param pos_soldat boolean
	 */
	public void setPos_soldat(boolean pos_soldat) {
		this.pos_soldat = pos_soldat;
	}

	/**
	 * Methode pour recuperer la coord x du clique de l'utilisateur
	 * @return int
	 */
	public int getX_pos_action_soldat() {
		return x_pos_action_soldat;
	}

	/**
	 * Methode pour recuperer la coord y du clique de l'utilisateur
	 * @return int
	 */
	public int getY_pos_action_soldat() {
		return y_pos_action_soldat;
	}

	/**
	 * Methode pour modifier la coord x du clique de l'utilisateur
	 * @param x_pos_action_soldat int
	 */
	public void setX_pos_action_soldat(int x_pos_action_soldat) {
		this.x_pos_action_soldat = x_pos_action_soldat;
	}

	/**
	 * Methode pour modifier la coord y du clique de l'utilisateur
	 * @param y_pos_action_soldat int
	 */
	public void setY_pos_action_soldat(int y_pos_action_soldat) {
		this.y_pos_action_soldat = y_pos_action_soldat;
	}

	/**
	 * Methode pour recuperer posPJ[][]
	 * @return Position[][]
	 */
	public Position[][] getPosPJ() {
		return posPJ;
	}

	/**
	 * Methode pour recuperer la carte
	 * @return Carte
	 */
	public Carte getCarte() {
		return carte;
	}

	/**
	 * Methode pour modifier la carte
	 * @param c Carte
	 */
	public void setCarte(Carte c) {
		carte = c;
	}

	
}
